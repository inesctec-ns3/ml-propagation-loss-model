// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.

#include "ns3/config.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/ml-propagation-loss-model.h"
#include "ns3/string.h"
#include "ns3/test.h"
#include "ns3/vector.h"

#include <iostream>
#include <sstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MlPropagationLossModelTestSuite");

/**
 * @ingroup ml-propagation-loss-model-test
 * @ingroup tests
 *
 * ML propagation loss model (position-based) test vector.
 */
struct MlplPositionTestVector
{
    /**
     * Constructor.
     *
     * @param txPosition Tx node position.
     * @param rxPosition Rx node position.
     * @param propagationLossDb Propagation loss (> 0) (dB).
     */
    MlplPositionTestVector(const Vector& txPosition,
                           const Vector& rxPosition,
                           double propagationLossDb)
        : txPosition(txPosition),
          rxPosition(rxPosition),
          propagationLossDb(propagationLossDb)
    {
    }

    Vector txPosition; //!< Tx node position
    Vector rxPosition; //!< Rx node position
    double propagationLossDb; //!< Propagation loss (> 0) (in dB)
};

/**
 * @ingroup ml-propagation-loss-model-test
 * @ingroup tests
 *
 * ML propagation loss model (position-based) test.
 */
class MlPropagationLossModelPositionTest : public TestCase
{
  public:
    MlPropagationLossModelPositionTest();

  private:
    void DoRun() override;

    TestVectors<MlplPositionTestVector> m_mlplTestVectors; //!< MLPL test vectors
};

MlPropagationLossModelPositionTest::MlPropagationLossModelPositionTest()
    : TestCase("Test the ML propagation loss model (position-based)")
{
}

void
MlPropagationLossModelPositionTest::DoRun()
{
    // Wi-Fi parameters from the corresponding dataset
    const std::string MLPL_DATASET = "position-dataset-example";
    const std::string MLPL_ML_INPUT_TYPE = "position";
    const std::string MLPL_ML_TRAINING_ALGORITHM = "xgb";

    const std::string MLPL_DATA_DIR = "./contrib/ml-propagation-loss-model/datasets/";
    const std::string FAST_FADING_CDF_PATH = MLPL_DATA_DIR + MLPL_DATASET + "/ml-model/" +
                                             MLPL_ML_INPUT_TYPE + "/" + MLPL_ML_TRAINING_ALGORITHM +
                                             "/fast-fading-ecdf.csv";

    // Tx power. The actual value is irrelevant, since the propagation loss is independent of
    // the Tx power.
    constexpr double TX_POWER_DBM = 1;

    // Propagation loss tolerance (in dB)
    constexpr double PROPAGATION_LOSS_TOLERANCE_DB = 1;

    // Create test vectors.
    // These values are generated manually considering the dataset.
    m_mlplTestVectors.Add(MlplPositionTestVector(Vector(0, 0, 0), Vector(2, 3.5, 0), 66));
    m_mlplTestVectors.Add(MlplPositionTestVector(Vector(0, 0, 0), Vector(11, 0, 0), 72));
    m_mlplTestVectors.Add(MlplPositionTestVector(Vector(0, 0, 0), Vector(14, 0, 0), 72));
    m_mlplTestVectors.Add(MlplPositionTestVector(Vector(0, 0, 0), Vector(21, 0, 0), 73));
    m_mlplTestVectors.Add(MlplPositionTestVector(Vector(0, 0, 0), Vector(23, 0, 0), 74));

    // ML propagation loss model
    Ptr<MlPropagationLossModel> lossModel =
        CreateObjectWithAttributes<MlPropagationLossModel>("FastFadingCdfPath",
                                                           StringValue(FAST_FADING_CDF_PATH));

    // Nodes
    Ptr<MobilityModel> txMobility = CreateObject<ConstantPositionMobilityModel>();
    Ptr<MobilityModel> rxMobility = CreateObject<ConstantPositionMobilityModel>();

    for (uint32_t testVectorIndex = 0; testVectorIndex < m_mlplTestVectors.GetN();
         testVectorIndex++)
    {
        const auto& testVector = m_mlplTestVectors.Get(testVectorIndex);

        // Set the Tx / Rx positions
        txMobility->SetPosition(testVector.txPosition);
        rxMobility->SetPosition(testVector.rxPosition);

        // Calculate the average Rx power
        constexpr uint32_t N_SAMPLES = 20;
        double avgPredictedRxPowerDbm = 0;

        for (uint32_t i = 0; i < N_SAMPLES; i++)
        {
            double predictedRxPowerDbm =
                lossModel->CalcRxPower(TX_POWER_DBM, txMobility, rxMobility);
            avgPredictedRxPowerDbm += predictedRxPowerDbm;
        }

        avgPredictedRxPowerDbm /= N_SAMPLES;

        double predictedPropagationLossDb = TX_POWER_DBM - avgPredictedRxPowerDbm;

        // Check that the average propagation loss calculated by the MLPL model matches the
        // dataset (within a tolerance)
        std::stringstream ss;

        ss << "Tx Position: " << testVector.txPosition << " | "
           << "Rx Position: " << testVector.rxPosition << " | "
           << "Real propagation loss: " << testVector.propagationLossDb << " dB" << " | "
           << "Predicted propagation loss: " << predictedPropagationLossDb << " dB" << " | "
           << "Propagation loss tolerance: " << PROPAGATION_LOSS_TOLERANCE_DB << " dB";

        std::cout << ss.str() << std::endl;

        NS_TEST_EXPECT_MSG_EQ_TOL(testVector.propagationLossDb,
                                  predictedPropagationLossDb,
                                  PROPAGATION_LOSS_TOLERANCE_DB,
                                  "Got unexpected propagation loss:" << ss.str());
    }
}

class MlPropagationLossModelTestSuite : public TestSuite
{
  public:
    MlPropagationLossModelTestSuite();
};

MlPropagationLossModelTestSuite::MlPropagationLossModelTestSuite()
    : TestSuite("ml-propagation-loss-model", TestSuite::Type::UNIT)
{
    AddTestCase(new MlPropagationLossModelPositionTest, TestCase::QUICK);
}

/// Test suite object
static MlPropagationLossModelTestSuite g_mlPropagationLossModelTestSuite;
