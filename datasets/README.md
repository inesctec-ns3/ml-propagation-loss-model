# MLPL Datasets

This folder contains the datasets to train the ML models of the path loss and fast-fading.

The full documentation about the datasets, including the format of the data, is available in the [MLPL Datasets](../doc/datasets.md) page.
