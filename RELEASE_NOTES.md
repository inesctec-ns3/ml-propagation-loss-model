# ML Propagation Loss Model for ns-3 Release Notes

This document contains the release notes of the ML based Propagation Loss (MLPL) model for ns-3. Each stable release contains a list of the major features and changes relative to the previous version.

## MLPL Version 1.0.3

Release date: N/A

* Standardize command-line arguments of the Python ML scripts in lowercase.
* Save the generated unique dataset in the `dataset-unique/` subdirectory. This subdirectory is ignored by Git, since it is an artifact of the ML model training.

## MLPL Version 1.0.2

Release date: 13 May 2024

* Fix compatibility issues with ns-3.41.
* General fixes and improvements.

## MLPL Version 1.0.1

Release date: 16 November 2023

* Update installation instructions to use ns3-ai v1.2.0, while MLPL does not support the latest version of ns3-ai.
* Add Python Black and isort code formatting.
* General fixes and improvements.

## MLPL Version 1.0

Release date: 19 October 2023

* First stable release of MLPL.
* General fixes and improvements.
* Update documentation.

## MLPL Version 0.2

Release date: 19 February 2023

* Implementation of the Position-based MLPL (P-MLPL) model.
  * More information about this model on the [paper](doc/publications.md) published in the Workshop on ns-3 (WNS3) 2023.
* General fixes and improvements.

## MLPL Version 0.1

Release date: 25 February 2022

* Initial release of the MLPL module for ns-3.
* Implementation of the Distance-based MLPL (D-MLPL) model.
  * More information about this model on the [paper](doc/publications.md) published in the Workshop on ns-3 (WNS3) 2022.
