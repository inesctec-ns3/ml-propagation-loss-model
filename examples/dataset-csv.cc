// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.
//
// Authors:
//   Eduardo Nuno Almeida [INESC TEC, Portugal]

#include "dataset-csv.h"

#include "ns3/csv-reader.h"
#include "ns3/vector.h"

#include <tuple>
#include <vector>

using namespace ns3;

std::size_t
DatasetCsv::GetNRows() const
{
    return txPositions.size();
}

std::tuple<Vector, Vector>
DatasetCsv::GetCsvRow(std::size_t csvRowIndex) const
{
    if (csvRowIndex >= txPositions.size())
    {
        NS_ABORT_MSG("Trying to access an invalid dataset CSV row (" << csvRowIndex << ")");
    }

    return {txPositions.at(csvRowIndex), rxPositions.at(csvRowIndex)};
}

void
DatasetCsv::LoadDatasetCsv(const std::string& filename)
{
    txPositions.clear();
    rxPositions.clear();

    CsvReader csvReader(filename);

    // Skip CSV header
    csvReader.FetchNextRow();

    while (csvReader.FetchNextRow())
    {
        if (csvReader.IsBlankRow())
        {
            continue;
        }

        // Read Tx position
        Vector txPosition;

        if (!csvReader.GetValue(0, txPosition.x))
        {
            NS_ABORT_MSG("Error reading field \"x_tx\" from the dataset CSV file");
        }
        if (!csvReader.GetValue(1, txPosition.y))
        {
            NS_ABORT_MSG("Error reading field \"y_tx\" from the dataset CSV file");
        }
        if (!csvReader.GetValue(2, txPosition.z))
        {
            NS_ABORT_MSG("Error reading field \"z_tx\" from the dataset CSV file");
        }

        txPositions.emplace_back(txPosition);

        // Read Rx position
        Vector rxPosition;

        if (!csvReader.GetValue(3, rxPosition.x))
        {
            NS_ABORT_MSG("Error reading field \"x_rx\" from the dataset CSV file");
        }
        if (!csvReader.GetValue(4, rxPosition.y))
        {
            NS_ABORT_MSG("Error reading field \"y_rx\" from the dataset CSV file");
        }
        if (!csvReader.GetValue(5, rxPosition.z))
        {
            NS_ABORT_MSG("Error reading field \"z_rx\" from the dataset CSV file");
        }

        rxPositions.emplace_back(rxPosition);
    }
}
