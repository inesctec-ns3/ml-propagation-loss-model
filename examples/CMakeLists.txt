build_lib_example(
  NAME ml-propagation-loss-model-throughput
  SOURCE_FILES
    ml-propagation-loss-model-throughput.cc
    dataset-csv.cc
  LIBRARIES_TO_LINK
    ${libml-propagation-loss-model}
    ${libapplications}
    ${libinternet}
    ${libmobility}
    ${libnetwork}
    ${libpropagation}
    ${libwifi}
    ${libns3-ai}
)
