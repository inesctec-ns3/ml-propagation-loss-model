// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.
//
// Authors:
//   Eduardo Nuno Almeida [INESC TEC, Portugal]

////////////////////////////////////////////////////////////////////////////////////
// ML Propagation Loss Model throughput example.
//
// This example shows how to use the ML propagation loss model.
// Moreover, this example simulates the scenario represented by a given dataset,
// calculating the average throughput received by the node.
//
// NOTES:
//   - This example only works for datasets with Tx / Rx positions. It does not work
//     for datasets with distances.
//   - This example uses the hard-coded Wi-Fi parameters of the position-dataset-example
//     (e.g., Wi-Fi standard, antenna gains, ...).
////////////////////////////////////////////////////////////////////////////////////

#include "dataset-csv.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/ml-propagation-loss-model.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/propagation-module.h"
#include "ns3/wifi-module.h"

#include <cmath>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MlPropagationLossModelThroughput");

///////////////////////////////////////////////////////////
// PARAMETERS AND GLOBAL VARIABLES
///////////////////////////////////////////////////////////

/// Monitoring time per position
const Time MONITORING_TIME_PER_POSITION = Seconds(5.0);

/// Warm-up time in a position before starting monitoring
const Time WARMUP_TIME_PER_POSITION = Seconds(1.0);

/// Total time per position
const Time TOTAL_TIME_PER_POSITION = WARMUP_TIME_PER_POSITION + MONITORING_TIME_PER_POSITION;

DatasetCsv g_datasetCsv; //!< Dataset CSV

Time g_monitoringStartTime; //!< Time when monitoring started
uint32_t g_rxBytes = 0; //!< Rx total bytes

std::ofstream g_resultsFileStream; //!< Results file stream

///////////////////////////////////////////////////////////
// PATHS
///////////////////////////////////////////////////////////

/**
 * Get the directory of a given dataset.
 *
 * @param dataset Dataset name.
 * @return Dataset directory.
 */
inline std::string
GetDatasetDirectory(const std::string& dataset)
{
    return "./contrib/ml-propagation-loss-model/"
           "datasets/" +
           dataset + "/";
}

/**
 * Get the path to the dataset positions file.
 *
 * @param dataset Dataset name.
 * @return Dataset positions file path.
 */
inline std::string
GetDatasetPositionsPath(const std::string& dataset)
{
    return GetDatasetDirectory(dataset) + "dataset-unique/"
                                          "propagation-loss-unique-dataset.csv";
}

/**
 * Get the path of the fast-fading CDF CSV file for a given ML training algorithm.
 *
 * @param dataset Dataset name.
 * @param mlTrainingAlgorithm ML training algorithm (e.g., xgb, svr).
 * @return Fast-fading CDF CSV file path.
 */
inline std::string
GetFastFadingCdfPath(const std::string& dataset, const std::string& mlTrainingAlgorithm)
{
    // clang-format off
    return GetDatasetDirectory(dataset) +
           "ml-model/" +
           "position/" +
           mlTrainingAlgorithm + "/"
           "fast-fading-ecdf.csv";
    // clang-format on
}

/**
 * Get the path of the results CSV file for a given propagation loss model and simulation run.
 *
 * @param dataset Dataset name.
 * @param lossModelStripped Propagation loss model, stripped of prefixes
 *                          (e.g., xgb, svr, friis, log-dist-1.7).
 * @param nRun Simulation run.
 * @return Path of the results CSV file.
 */
inline std::string
GetResultsCsvPath(const std::string& dataset, const std::string& lossModelStripped, uint32_t nRun)
{
    // clang-format off
    return GetDatasetDirectory(dataset) +
           "results/" +
           "position/" +
           lossModelStripped + "/" +
           ("throughput-" + std::to_string(nRun) + ".csv");
    // clang-format on
}

///////////////////////////////////////////////////////////
// AUXILIARY FUNCTIONS
///////////////////////////////////////////////////////////

/**
 * Get the position of a given node.
 *
 * @param nodeId Node ID.
 * @return Node position.
 */
Vector
GetNodePosition(uint32_t nodeId)
{
    Ptr<ConstantPositionMobilityModel> nodeMobility =
        NodeList::GetNode(nodeId)->GetObject<ConstantPositionMobilityModel>();

    return nodeMobility->GetPosition();
}

/**
 * Set the position of a given node.
 *
 * @param nodeId Node ID.
 * @param position Node position.
 */
void
SetNodePosition(uint32_t nodeId, const Vector& position)
{
    Ptr<ConstantPositionMobilityModel> nodeMobility =
        NodeList::GetNode(nodeId)->GetObject<ConstantPositionMobilityModel>();

    return nodeMobility->SetPosition(position);
}

/**
 * Reset the throughput counters and start a new monitoring session for a given position.
 */
void
ResetCounters()
{
    g_rxBytes = 0;
    g_monitoringStartTime = Simulator::Now();
}

/**
 * Calculate the current throughput and update the results file.
 */
void
UpdateThroughputResultsFile()
{
    Vector txPosition = GetNodePosition(0);
    Vector rxPosition = GetNodePosition(1);

    // Calculate the throughput
    Time duration = Simulator::Now() - g_monitoringStartTime;
    double throughputKbps = ((g_rxBytes * 8.0) / duration.GetSeconds()) / 1e3;

    // Update the results file
    std::stringstream ss;

    // Write CSV row
    ss << txPosition.x << "," << txPosition.y << "," << txPosition.z << "," // Tx position
       << rxPosition.x << "," << rxPosition.y << "," << rxPosition.z << "," // Rx position
       << throughputKbps; // Throughput

    NS_LOG_INFO(ss.str());

    g_resultsFileStream << ss.str() << std::endl;
}

void
RxPacketCallback(Ptr<const Packet> packet, const Address& /*sourceAddress*/)
{
    g_rxBytes += packet->GetSize();
}

/**
 * Start monitoring the throughput for a given set os Tx / Rx positions.
 *
 * @param datasetCsvRowIndex The index of the dataset CSV row.
 */
void
StartThroughputMonitoring(uint32_t datasetCsvRowIndex)
{
    // Set the nodes positions
    auto [txPosition, rxPosition] = g_datasetCsv.GetCsvRow(datasetCsvRowIndex);

    SetNodePosition(0, txPosition);
    SetNodePosition(1, rxPosition);

    // Schedule the start of a new monitoring session (after Minstrel stabilizes)
    Simulator::Schedule(WARMUP_TIME_PER_POSITION, &ResetCounters);

    // Schedule the calculation and update of the throughput
    Simulator::Schedule(WARMUP_TIME_PER_POSITION + MONITORING_TIME_PER_POSITION,
                        &UpdateThroughputResultsFile);

    // Schedule the next monitoring session
    if (datasetCsvRowIndex + 1 < g_datasetCsv.GetNRows())
    {
        Simulator::Schedule(WARMUP_TIME_PER_POSITION + MONITORING_TIME_PER_POSITION,
                            &StartThroughputMonitoring,
                            datasetCsvRowIndex + 1);
    }
}

double
CalculateFriisLossDb(double logDistD0, double frequencyMhz)
{
    constexpr double C = 3e8;
    double lossDb = -20.0 * std::log10(C / (4.0 * M_PI * frequencyMhz * 1e6 * logDistD0));

    return lossDb;
}

///////////////////////////////////////////////////////////
// MAIN
///////////////////////////////////////////////////////////
int
main(int argc, char* argv[])
{
    std::string lossModel;
    std::string dataset;
    uint32_t nRun = 1;
    bool verbose = false;

    CommandLine cmd;
    cmd.AddValue("lossModel",
                 "Propagation loss model {mlpl-xgb, mlpl-svr, friis, log-dist-1.7}",
                 lossModel);
    cmd.AddValue("dataset",
                 "Dataset used to run this test (e.g., \"position-dataset-example\"). "
                 "It must be a position-based dataset.",
                 dataset);
    cmd.AddValue("nRun", "Simulation run seed (for confidence interval)", nRun);
    cmd.AddValue("verbose", "Enable verbose output", verbose);
    cmd.Parse(argc, argv);

    // Process and validate arguments
    NS_ABORT_MSG_IF(dataset.empty(), "--dataset argument is mandatory");

    // Define RNG seeds
    SeedManager::SetSeed(1);
    SeedManager::SetRun(nRun);

    if (verbose)
    {
        LogComponentEnable("MlPropagationLossModelThroughput", LOG_LEVEL_INFO);
        LogComponentEnable("MlPropagationLossModel", LOG_INFO);
    }

    // Read dataset CSV file
    g_datasetCsv.LoadDatasetCsv(GetDatasetPositionsPath(dataset));

    // Nodes (0: Tx | 1: Rx)
    NodeContainer nodes(2);

    MobilityHelper mobility;
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

    Ptr<ListPositionAllocator> positionAllocator = CreateObject<ListPositionAllocator>();
    positionAllocator->Add(Vector(0, 0, 0));
    positionAllocator->Add(Vector(0, 0, 0));

    mobility.SetPositionAllocator(positionAllocator);
    mobility.Install(nodes);

    // Wi-Fi parameters from the corresponding dataset.
    // Currently, they are hard-coded for the position-dataset-example.
    constexpr uint32_t FREQUENCY_MHZ = 5220;
    constexpr uint32_t CHANNEL_NUMBER = (FREQUENCY_MHZ - 5000) / 5;
    constexpr uint32_t BANDWIDTH_MHZ = 20;

    constexpr double TX_POWER_DBM = 1;
    constexpr double TX_GAIN_DBI = -7;
    constexpr double RX_GAIN_DBI = -7;

    // Global wireless configurations
    Config::SetDefault("ns3::ThresholdPreambleDetectionModel::MinimumRssi", DoubleValue(-90));

    // Wi-Fi configurations
    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211a);
    wifi.SetRemoteStationManager("ns3::MinstrelWifiManager");

    YansWifiChannelHelper wifiChannel;
    wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");

    // Propagation loss model
    std::string lossModelStripped;

    if (lossModel == "mlpl-xgb" || lossModel == "mlpl-svr")
    {
        lossModelStripped = lossModel.substr(lossModel.find('-') + 1);

        wifiChannel.AddPropagationLoss(
            "ns3::MlPropagationLossModel",
            "FastFadingCdfPath",
            StringValue(GetFastFadingCdfPath(dataset, lossModelStripped)),
            "PathLossCache",
            BooleanValue(true));
    }
    else if (lossModel == "friis")
    {
        lossModelStripped = lossModel;

        wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                       "Frequency",
                                       DoubleValue(FREQUENCY_MHZ * 1e6));
    }
    else if (lossModel == "log-dist-1.7")
    {
        constexpr double LOG_DIST_D0 = 1.0;
        const double LOG_DIST_PL0 = CalculateFriisLossDb(LOG_DIST_D0, FREQUENCY_MHZ);

        lossModelStripped = lossModel;

        wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
                                       "Exponent",
                                       DoubleValue(1.7),
                                       "ReferenceDistance",
                                       DoubleValue(LOG_DIST_D0),
                                       "ReferenceLoss",
                                       DoubleValue(LOG_DIST_PL0));
    }
    else
    {
        NS_ABORT_MSG("Unsupported propagation loss model: " << lossModel);
    }

    // Wi-Fi PHY
    std::stringstream wifiPhyChannelSettingsSs;
    wifiPhyChannelSettingsSs << "{" << CHANNEL_NUMBER << "," << BANDWIDTH_MHZ << ","
                             << "BAND_5GHZ, 0}";

    YansWifiPhyHelper wifiPhy;
    wifiPhy.Set("ChannelSettings", StringValue(wifiPhyChannelSettingsSs.str()));
    wifiPhy.Set("TxPowerStart", DoubleValue(TX_POWER_DBM));
    wifiPhy.Set("TxPowerEnd", DoubleValue(TX_POWER_DBM));
    wifiPhy.Set("TxGain", DoubleValue(TX_GAIN_DBI));
    wifiPhy.Set("RxGain", DoubleValue(RX_GAIN_DBI));
    wifiPhy.SetChannel(wifiChannel.Create());

    // Wi-Fi MAC
    WifiMacHelper wifiMac;
    wifiMac.SetType("ns3::AdhocWifiMac");
    NetDeviceContainer netDevices = wifi.Install(wifiPhy, wifiMac, nodes);

    // Internet stack
    InternetStackHelper internetStack;
    internetStack.Install(nodes);

    Ipv4AddressHelper ipv4AddressHelper;
    ipv4AddressHelper.SetBase("10.0.0.0", "/24");
    Ipv4InterfaceContainer ipv4Interfaces = ipv4AddressHelper.Assign(netDevices);

    // Applications
    constexpr uint16_t SERVER_PORT = 9;

    const Time RX_APP_START_TIME = Seconds(0.5);
    const Time TX_APP_START_TIME = Seconds(1.0);

    OnOffHelper onOffHelper("ns3::UdpSocketFactory",
                            InetSocketAddress(ipv4Interfaces.GetAddress(1), SERVER_PORT));
    onOffHelper.SetConstantRate(DataRate("54Mbps"), 1400); // Saturate the wireless link
    onOffHelper.SetAttribute("StartTime", TimeValue(TX_APP_START_TIME));
    ApplicationContainer onOffApps = onOffHelper.Install(nodes.Get(0));

    PacketSinkHelper packetSinkHelper("ns3::UdpSocketFactory",
                                      InetSocketAddress(Ipv4Address::GetAny(), SERVER_PORT));
    packetSinkHelper.SetAttribute("StartTime", TimeValue(RX_APP_START_TIME));
    ApplicationContainer packetSinkApps = packetSinkHelper.Install(nodes.Get(1));

    // Create the results file
    const std::string RESULTS_CSV_PATH = GetResultsCsvPath(dataset, lossModelStripped, nRun);

    g_resultsFileStream.open(RESULTS_CSV_PATH, std::ios_base::out);

    if (!g_resultsFileStream.is_open())
    {
        NS_ABORT_MSG("Error opening results file: " << RESULTS_CSV_PATH);
    }

    // Write CSV header
    constexpr auto csvHeader = "x_tx,y_tx,z_tx,x_rx,y_rx,z_rx,throughput_kbps";
    g_resultsFileStream << csvHeader << std::endl;

    NS_LOG_INFO(csvHeader);

    // Schedule throughput monitoring
    Config::ConnectWithoutContext("/NodeList/1/ApplicationList/0/$ns3::PacketSink/Rx",
                                  MakeCallback(&RxPacketCallback));

    Simulator::Schedule(TX_APP_START_TIME, &StartThroughputMonitoring, 0);

    // Start simulation
    const std::size_t nPositions = g_datasetCsv.GetNRows();

    const Time simulationStopTime =
        TX_APP_START_TIME + (nPositions * TOTAL_TIME_PER_POSITION) + Seconds(1.0);

    Simulator::Stop(simulationStopTime);
    Simulator::Run();
    Simulator::Destroy();

    g_resultsFileStream.close();

    return 0;
}
