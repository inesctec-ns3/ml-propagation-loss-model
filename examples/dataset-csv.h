// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.
//
// Authors:
//   Eduardo Nuno Almeida [INESC TEC, Portugal]

#include "ns3/vector.h"

#include <tuple>
#include <vector>

using namespace ns3;

/**
 * Dataset CSV file.
 */
class DatasetCsv
{
  public:
    /**
     * Constructor.
     */
    DatasetCsv() = default;

    /**
     * Get the number of CSV rows.
     *
     * @return Number of CSV rows.
     */
    std::size_t GetNRows() const;

    /**
     * Get a row of the CSV dataset.
     *
     * @param csvRowIndex Index of the CSV row.
     * @return Tuple: [Tx position, Rx position]
     */
    std::tuple<Vector, Vector> GetCsvRow(std::size_t csvRowIndex) const;

    /**
     * Load the dataset CSV file.
     *
     * @param filename Dataset filename.
     * @return A vector of positions and respective average throughput.
     */
    void LoadDatasetCsv(const std::string& filename);

  private:
    std::vector<Vector> txPositions; //!< Vector of Tx positions
    std::vector<Vector> rxPositions; //!< Vector of Rx positions
};
