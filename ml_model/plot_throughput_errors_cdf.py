# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Module for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Plot the throughput errors CDF.
"""

import argparse
import os
from typing import List

import numpy as np
import pandas as pd

from mlpl import paths, results
from mlpl.mlpl_model import MlplModel


def plot_throughput_errors_cdf(dataset: str) -> None:
    """
    Plot the throughput errors CDF.

    Args
    ----
        dataset Dataset used to train the ML model.
    """

    # Parameters
    mlpl_model = MlplModel.POSITION

    if mlpl_model == MlplModel.DISTANCE:
        mlpl_legend = "D-MLPL"
    elif mlpl_model == MlplModel.POSITION:
        mlpl_legend = "P-MLPL"
    else:
        raise ValueError(f"Invalid MLPL model: {mlpl_model}")

    LOSS_LABELS = [
        f"{mlpl_legend} (XGB)",
        f"{mlpl_legend} (SVR)",
        "Friis",
        "LogDist-1.7",
    ]

    LOSS_DIRECTORIES = [
        "xgb",
        "svr",
        "friis",
        "log-dist-1.7",
    ]

    results_path = paths.get_results_path(dataset, mlpl_model)

    # Read the throughputs and calculate the errors
    throughputs_kbps_errors_df: List[pd.DataFrame] = []

    real_throughput_kbps_df = pd.read_csv(
        os.path.join(paths.get_unique_dataset_path(dataset), "propagation-loss-unique-dataset.csv"),
        dtype=np.float32,
    )

    for loss_dir in LOSS_DIRECTORIES:
        # Calculate mean value (for confidence intervals)
        N_THROUGHPUT_FILES = 10
        throughputs_kbps_concat_df: pd.DataFrame = pd.DataFrame()

        for i in range(1, N_THROUGHPUT_FILES + 1):
            throughput_kbps_df = pd.read_csv(
                os.path.join(results_path, loss_dir, f"throughput-{i}.csv"),
                dtype=np.float32,
            )

            throughputs_kbps_concat_df = pd.concat([throughputs_kbps_concat_df, throughput_kbps_df])

        mean_throughput_kbps_df = throughputs_kbps_concat_df.groupby(
            throughputs_kbps_concat_df.index
        ).mean()

        # Calculate the throughput error
        throughputs_kbps_errors_df.append(
            results.calculate_throughput_error(
                real_throughput_kbps_df,
                mean_throughput_kbps_df,
                mlpl_model,
            )
        )

    # Plots
    results.plot_throughput_errors_cdf(
        throughputs_kbps_errors_df,
        LOSS_LABELS,
        plot_absolute_errors=True,
        results_path=results_path,
    )

    results.plot_throughput_errors_cdf(
        throughputs_kbps_errors_df,
        LOSS_LABELS,
        plot_absolute_errors=False,
        results_path=results_path,
    )


if __name__ == "__main__":
    # Argument parser
    parser = argparse.ArgumentParser(description="Plot the throughput errors CDF.")

    parser.add_argument(
        "--dataset",
        action="store",
        type=str,
        required=True,
        help="Dataset used to train the ML model",
    )

    args = parser.parse_args()

    # Main
    plot_throughput_errors_cdf(
        args.dataset,
    )
