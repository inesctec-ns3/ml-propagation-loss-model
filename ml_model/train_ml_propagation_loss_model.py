# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.

"""
Train the ML propagation loss model.
"""

import argparse
import sys
from typing import List

import pandas as pd

from mlpl import dataset_manager, ml_models, path_loss, paths, results
from mlpl.ml_algorithm import MlAlgorithm
from mlpl.mlpl_model import MlplModel
from mlpl.wifi_parameters import WifiParameters


def train_ml_propagation_loss_model(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> None:
    """
    Train the ML propagation loss model.

    Args
    ----
        dataset Dataset used to train the ML model.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.
    """

    print(f"Training MLPL model: {dataset=}, {mlpl_model=}, {ml_algorithm=}")

    # Make the subdirectories
    try:
        paths.make_subdirectories(dataset, mlpl_model, ml_algorithm)
    except FileNotFoundError as e:
        sys.exit(f"Error creating the subdirectories: {e}")

    # Load dataset and split into train / test sets
    try:
        (
            propagation_loss_df,
            path_loss_df,
            fast_fading_df,
        ) = dataset_manager.load_propagation_loss_dataset(dataset, mlpl_model)

        (
            propagation_loss_train_df,
            propagation_loss_test_df,
            path_loss_train_df,
            path_loss_test_df,
            fast_fading_train_df,
            fast_fading_test_df,
        ) = dataset_manager.split_dataset_into_train_test(
            propagation_loss_df,
            path_loss_df,
            fast_fading_df,
        )

        dataset_manager.create_unique_positions_dataset(dataset, mlpl_model)

    except FileNotFoundError as e:
        sys.exit(f"Wi-Fi parameters JSON file not found. {e}")

    except Exception as e:
        sys.exit(f"Error loading the propagation loss dataset: {e}")

    # ML model training
    try:
        ml_models.train_path_loss_ml_model(
            path_loss_train_df,
            path_loss_test_df,
            dataset,
            mlpl_model,
            ml_algorithm,
        )

        ml_models.train_fast_fading_ml_model(
            fast_fading_df,
            dataset,
            mlpl_model,
            ml_algorithm,
        )

        (
            propagation_loss_predictor,
            path_loss_predictor,
            fast_fading_predictor,
        ) = ml_models.get_propagation_loss_ml_model_predictors(dataset, mlpl_model, ml_algorithm)

    except FileExistsError as e:
        sys.exit(f"Error training the ML models: the ML model already exists. {e}")

    except Exception as e:
        sys.exit(f"Error training the ML models: {e}")

    # Calculate the ML models predictions and errors
    (
        propagation_loss_test_error_df,
        propagation_loss_test_prediction_df,
    ) = results.calculate_ml_propagation_loss_error(
        propagation_loss_test_df,
        propagation_loss_predictor,
        mlpl_model,
    )

    (
        path_loss_test_error_df,
        path_loss_test_prediction_df,
    ) = results.calculate_ml_propagation_loss_error(
        path_loss_test_df,
        path_loss_predictor,
        mlpl_model,
    )

    (
        fast_fading_test_error_df,
        fast_fading_test_prediction_df,
    ) = results.calculate_ml_propagation_loss_error(
        fast_fading_test_df,
        fast_fading_predictor,
        mlpl_model,
    )

    # Calculate the path loss errors for the baselines
    # Requires a Wi-Fi parameters file
    baseline_losses_df: List[pd.DataFrame] = []
    baseline_labels: List[str] = []
    baseline_loss_errors_df: List[pd.DataFrame] = []

    try:
        wifi_parameters = WifiParameters.from_json(dataset)

        propagation_loss_input_df_columns = (
            dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)
        )

        (baseline_losses_df, baseline_labels) = path_loss.calculate_baseline_path_losses(
            propagation_loss_test_df[propagation_loss_input_df_columns],
            wifi_parameters,
        )

        for loss_df in baseline_losses_df:
            baseline_loss_errors_df.append(
                results.calculate_propagation_loss_error(
                    loss_df[propagation_loss_input_df_columns],
                    propagation_loss_test_df["loss_db"].to_numpy(),
                    loss_df["loss_db"].to_numpy(),
                )
            )

    except FileNotFoundError as e:
        print("WARNING: Wi-Fi parameters JSON file not found.", e)
        print("Skipping plots for the propagation loss baselines")

    # Plot results
    results_path = paths.get_results_path(dataset, mlpl_model, ml_algorithm)

    if mlpl_model == MlplModel.DISTANCE:
        mlpl_legend = "D-MLPL"
    elif mlpl_model == MlplModel.POSITION:
        mlpl_legend = "P-MLPL"
    else:
        raise ValueError(f"Invalid MLPL model: {mlpl_model}")

    results.plot_dataset_input_data(
        propagation_loss_df,
        mlpl_model,
        results_path,
    )

    if mlpl_model == MlplModel.DISTANCE:
        results.plot_propagation_loss_distance_predictions(
            [propagation_loss_test_prediction_df] + baseline_losses_df + [propagation_loss_test_df],
            [f"{mlpl_legend} ({ml_algorithm})"] + baseline_labels + ["Real"],
            results_path,
        )

    if mlpl_model == MlplModel.DISTANCE:
        results.plot_propagation_loss_distance_errors(
            [propagation_loss_test_error_df] + baseline_loss_errors_df,
            [f"{mlpl_legend} ({ml_algorithm})"] + baseline_labels,
            results_path,
        )

    results.plot_propagation_loss_errors_cdf(
        [propagation_loss_test_error_df] + baseline_loss_errors_df,
        [f"{mlpl_legend} ({ml_algorithm})"] + baseline_labels,
        plot_absolute_errors=True,
        results_path=results_path,
    )

    results.plot_propagation_loss_errors_cdf(
        [propagation_loss_test_error_df] + baseline_loss_errors_df,
        [f"{mlpl_legend} ({ml_algorithm})"] + baseline_labels,
        plot_absolute_errors=False,
        results_path=results_path,
    )

    print("Finished training ML model")


if __name__ == "__main__":
    # Argument parser
    parser = argparse.ArgumentParser(
        description="Train the ML propagation loss model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--dataset",
        action="store",
        type=str,
        required=True,
        help="Name of the dataset used to train the ML model (equal to the name of the folder)",
    )

    parser.add_argument(
        "--mlpl-model",
        action="store",
        type=MlplModel,
        required=True,
        choices=list(MlplModel),
        help="MLPL model (Distance-MLPL or Position-MLPL)",
    )

    parser.add_argument(
        "--ml-algorithm",
        action="store",
        type=MlAlgorithm,
        choices=list(MlAlgorithm),
        default=MlAlgorithm.SVR,
        help="ML algorithm used to train the MLPL model",
    )

    args = parser.parse_args()

    # Main
    train_ml_propagation_loss_model(
        args.dataset,
        args.mlpl_model,
        args.ml_algorithm,
    )
