# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.

"""
Run the ML propagation loss model.
"""

import argparse
import ctypes
import sys

import numpy as np
import pandas as pd
import py_interface

from mlpl import ml_models, results
from mlpl.ml_algorithm import MlAlgorithm
from mlpl.mlpl_model import MlplModel

# ns3-ai parameters
MEMPOOL_KEY = 1234  # Memory pool key, arbitrary integer large than 1000
MEM_SIZE = 4096  # Memory pool size (in bytes)
MEMBLOCK_KEY = 3005  # Memory block key (must match ID in the ns-3 scripts)


class MlFeature(py_interface.Structure):
    """ML model input features."""

    _pack_ = 1
    _fields_ = [
        ("x_tx", ctypes.c_double),  # X coordinates of the Tx node (in m)
        ("y_tx", ctypes.c_double),  # Y coordinates of the Tx node (in m)
        ("z_tx", ctypes.c_double),  # Z coordinates of the Tx node (in m)
        ("x_rx", ctypes.c_double),  # X coordinates of the Rx node (in m)
        ("y_rx", ctypes.c_double),  # Y coordinates of the Rx node (in m)
        ("z_rx", ctypes.c_double),  # Z coordinates of the Rx node (in m)
    ]


class MlPredicted(py_interface.Structure):
    """ML model output prediction."""

    _pack_ = 1
    _fields_ = [
        # Predicted path loss (> 0) for the Tx / Rx positions (in dB)
        ("path_loss_db", ctypes.c_double),
    ]


class MlTarget(py_interface.Structure):
    """ML model output target (real value)."""

    _pack_ = 1
    _fields_ = [
        # Target (real) path loss (> 0) for the Tx / Rx positions (in dB)
        ("path_loss_db", ctypes.c_double),
    ]


def run_ml_propagation_loss_model(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> None:
    """
    Run the ML propagation loss model.

    Args
    ----
        dataset Dataset used to train the ML model.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.
    """

    # Get the ML propagation loss predictor
    print(f">> Starting ML propagation loss model: {dataset=}, {mlpl_model=}, {ml_algorithm=}")

    try:
        (_, path_loss_predictor, _) = ml_models.get_propagation_loss_ml_model_predictors(
            dataset,
            mlpl_model,
            ml_algorithm,
        )

    except Exception as e:
        sys.exit(f"Error getting the ML propagation loss model: {e}")

    # Start ns3-ai
    py_interface.Init(MEMPOOL_KEY, MEM_SIZE)

    print(">> Waiting for the ns-3 simulation to start...")
    print("Press CTRL-C to exit")

    dl = py_interface.Ns3AIDL(MEMBLOCK_KEY, MlFeature, MlPredicted, MlTarget)
    ml_model_started = False

    try:
        while True:
            if dl.isFinish():
                break

            with dl as data:
                if not data:
                    break

                if not ml_model_started:
                    ml_model_started = True
                    print(">> Started the ML propagation loss model")

                # Get the Tx / Rx nodes positions from ns-3
                positions = np.array(
                    [
                        [
                            data.feat.x_tx,
                            data.feat.y_tx,
                            data.feat.z_tx,
                            data.feat.x_rx,
                            data.feat.y_rx,
                            data.feat.z_rx,
                        ],
                    ],
                    dtype=np.float32,
                )

                # Determine the input of the ML propagation loss model.
                # When using distance-based MLPL, convert the positions to distance.
                if mlpl_model == MlplModel.DISTANCE:
                    distance_m = results.calculate_distance_from_positions(
                        pd.DataFrame(
                            positions,
                            columns=["x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx"],
                        )
                    )

                    ml_model_input = distance_m.reshape(-1, 1)

                elif mlpl_model == MlplModel.POSITION:
                    ml_model_input = positions

                else:
                    raise ValueError(f"Invalid MLPL model: {mlpl_model}")

                # Predict path loss with the ML propagation loss model
                path_loss_db = path_loss_predictor(ml_model_input)
                data.pred.path_loss_db = path_loss_db

    except KeyboardInterrupt:
        pass

    except Exception as e:
        print(">> An exception occurred:", e)

    py_interface.FreeMemory()
    print(">> Finished the ML propagation loss model")


if __name__ == "__main__":
    # Argument parser
    parser = argparse.ArgumentParser(description="Run the ML propagation loss model")

    parser.add_argument(
        "--dataset",
        action="store",
        type=str,
        required=True,
        help="Name of the dataset to run the ML model (equal to the name of the folder)",
    )

    parser.add_argument(
        "--mlpl-model",
        action="store",
        type=MlplModel,
        required=True,
        choices=list(MlplModel),
        help="MLPL model (Distance-MLPL or Position-MLPL)",
    )

    parser.add_argument(
        "--ml-algorithm",
        action="store",
        type=MlAlgorithm,
        choices=list(MlAlgorithm),
        default=MlAlgorithm.SVR,
        help="ML algorithm used to train the MLPL model",
    )

    args = parser.parse_args()

    # Main
    run_ml_propagation_loss_model(
        args.dataset,
        args.mlpl_model,
        args.ml_algorithm,
    )
