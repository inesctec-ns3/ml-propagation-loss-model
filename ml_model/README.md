# Machine Learning Model Scripts

This folder contains the scripts to train and run the ML models for the path loss and fast-fading.

The full documentation about the training and running of the ML models supporting the MLPL model is available in this page: [ML Model Training](../doc/ml-model-training.md).

## Directory Structure and Scripts

* `mlpl/`: MLPL Python module scripts
* `train_ml_propagation_loss_model.py`: Script to train the MLPL model.
* `run_ml_propagation_loss_model.py`: Script to run the MLPL model.
* `plot_throughput_errors_cdf.py`: Script to plot the throughput errors CDF using a trained ML model.
