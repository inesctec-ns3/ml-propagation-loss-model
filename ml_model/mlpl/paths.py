# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Paths manager.
Contains functions to get the paths to important directories and manage them.
"""

import os
from typing import Union

from .ml_algorithm import MlAlgorithm
from .mlpl_model import MlplModel

# Parameters
_DATA_PATH = "datasets"


def get_dataset_path(dataset: str) -> str:
    """
    Get the path to the dataset.

    Args
    ----
        dataset Dataset.

    Returns
    -------
        Dataset path.
    """

    return os.path.join(_DATA_PATH, dataset, "dataset")


def get_unique_dataset_path(dataset: str) -> str:
    """
    Get the path to the unique dataset.

    Args
    ----
        dataset Dataset.

    Returns
    -------
        Unique dataset path.
    """

    return os.path.join(_DATA_PATH, dataset, "dataset-unique")


def get_ml_model_path(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> str:
    """
    Get the path to the ML model.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm.

    Returns
    -------
        ML model path.
    """

    return os.path.join(_DATA_PATH, dataset, "ml-model", mlpl_model.value, ml_algorithm.value)


def get_results_path(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: Union[MlAlgorithm, None] = None,
) -> str:
    """
    Get the path to the results.

    @param dataset Dataset.
    @param mlpl_model MLPL model.
    @param ml_algorithm ML training algorithm. If omitted, get the base directory.
    @return Results path.
    """

    results_path = os.path.join(_DATA_PATH, dataset, "results", mlpl_model.value)

    if ml_algorithm:
        results_path = os.path.join(results_path, ml_algorithm.value)

    return results_path


def make_subdirectories(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> None:
    """
    Create the subdirectories for the ML models and results.
    If they already exist, do nothing.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm.
    """

    dataset_path = get_dataset_path(dataset)

    if not os.path.exists(dataset_path):
        raise ValueError(f"Dataset directory {dataset_path} not found")

    os.makedirs(get_unique_dataset_path(dataset), exist_ok=True)
    os.makedirs(get_ml_model_path(dataset, mlpl_model, ml_algorithm), exist_ok=True)
    os.makedirs(get_results_path(dataset, mlpl_model, ml_algorithm), exist_ok=True)
