# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.

"""
ML models.
"""

import os
import pickle
from typing import Callable, Tuple

import numpy as np
import pandas as pd
import sklearn
import sklearn.compose
import sklearn.model_selection
import sklearn.pipeline
import sklearn.preprocessing
import sklearn.svm
import xgboost

from . import dataset_manager, fast_fading, paths
from .ml_algorithm import MlAlgorithm
from .mlpl_model import MlplModel


#################################################
# COMPLETE PROPAGATION LOSS ML MODEL
#################################################
def get_propagation_loss_ml_model_predictors(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> Tuple[
    Callable[[np.ndarray], np.ndarray],
    Callable[[np.ndarray], np.ndarray],
    Callable[[np.ndarray], np.ndarray],
]:
    """
    Get the complete propagation loss ML model, consisting of the path loss, the fast-fading
    and the sum of both.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.

    Returns
    -------
        Tuple of callable functions to predict the [propagation_loss, path_loss, fast_fading]
        based on a "distance_m" value as input.
    """

    print("Getting the propagation loss ML model")

    path_loss_predictor = get_path_loss_ml_model_predictor(dataset, mlpl_model, ml_algorithm)
    fast_fading_predictor = get_fast_fading_ml_model_predictor(dataset, mlpl_model, ml_algorithm)

    def predict_propagation_loss(propagation_loss_input: np.ndarray) -> np.ndarray:
        """
        Predict the total propagation loss component for a given input (distance_m),
        which is the sum of the path loss and the fast-fading.

        Args
        ----
            propagation_loss_input np.ndarray with the input to the propagation loss model (distance_m).

        Returns
        -------
            np.ndarray with the propagation loss predictions.
        """

        path_loss_estimation = path_loss_predictor(propagation_loss_input)
        fast_fading_estimation = fast_fading_predictor(propagation_loss_input)
        propagation_loss_estimation = path_loss_estimation + fast_fading_estimation

        return propagation_loss_estimation

    return (predict_propagation_loss, path_loss_predictor, fast_fading_predictor)


#################################################
# PATH LOSS ML MODEL
#################################################
def train_path_loss_ml_model(
    path_loss_train_df: pd.DataFrame,
    path_loss_test_df: pd.DataFrame,
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> None:
    """
    Train the path loss ML model.

    Args
    ----
        path_loss_train_df Path loss training set DataFrame.
        path_loss_test_df Path loss test set DataFrame.
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.
    """

    print("Training the path loss ML model")

    # Build ML model
    if ml_algorithm == MlAlgorithm.XGB:
        estimator = xgboost.XGBRegressor(objective="reg:squarederror")
    elif ml_algorithm == MlAlgorithm.SVR:
        estimator = sklearn.svm.SVR()
    else:
        raise ValueError(f"Invalid ML algorithm: {ml_algorithm}")

    pipeline = sklearn.pipeline.make_pipeline(
        sklearn.preprocessing.StandardScaler(),
        estimator,
    )

    ml_model = sklearn.compose.TransformedTargetRegressor(
        regressor=pipeline,
        transformer=sklearn.preprocessing.StandardScaler(),
    )

    # Train ML model and calculate the precision
    df_columns = dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)

    ml_model.fit(
        path_loss_train_df[df_columns].to_numpy(),
        path_loss_train_df[["loss_db"]].to_numpy(),
    )

    x_test = path_loss_test_df[df_columns].to_numpy()
    y_test_true = path_loss_test_df[["loss_db"]].to_numpy()
    y_test_pred = ml_model.predict(x_test)

    ml_model_score = ml_model.score(x_test, y_test_true)
    ml_model_mse = np.mean((y_test_true - y_test_pred) ** 2)

    print(f"Path loss ML model score: {ml_model_score:.3f}")
    print(f"Path loss ML model MSE: {ml_model_mse:.3f}")

    # Save ML model accuracy
    with open(
        os.path.join(
            paths.get_results_path(dataset, mlpl_model, ml_algorithm),
            "path-loss-ml-model-precision.txt",
        ),
        "wt",
        encoding="utf-8",
    ) as f:
        f.writelines(
            [
                f"Score: {ml_model_score:.3f}\n",
                f"MSE: {ml_model_mse:.3f}\n",
            ]
        )

    # Save ML model
    with open(_get_path_loss_ml_model_path(dataset, mlpl_model, ml_algorithm), "wb") as f:
        pickle.dump(ml_model, f)


def get_path_loss_ml_model_predictor(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> Callable[[np.ndarray], np.ndarray]:
    """
    Get the path loss ML model prediction function.
    This function predicts the path loss (dB) based on a distance value (m) as input.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.

    Returns
    -------
        Callable function to predict the path loss.
    """

    print("Getting the path loss ML model")

    with open(_get_path_loss_ml_model_path(dataset, mlpl_model, ml_algorithm), "rb") as f:
        path_loss_ml_model = pickle.load(f)

    def predict_path_loss(path_loss_input: np.ndarray) -> np.ndarray:
        """
        Predict the path loss component for a given input.

        @param path_loss_input np.ndarray with the input to the path loss model.
        @return np.ndarray with the path loss prediction.
        """

        path_loss_prediction = path_loss_ml_model.predict(path_loss_input)
        path_loss_prediction = path_loss_prediction.squeeze()

        return path_loss_prediction

    return predict_path_loss


#################################################
# FAST-FADING ML MODEL
#################################################
def train_fast_fading_ml_model(
    fast_fading_df: pd.DataFrame,
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> None:
    """
    Train the fast-fading ML model.

    Args
    ----
        fast_fading_df Fast-fading DataFrame.
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.
    """

    print("Training the fast-fading ML model")

    results_path = paths.get_results_path(dataset, mlpl_model, ml_algorithm)

    # Train ML model
    (fast_fading_ecdf, fast_fading_inverse_ecdf, _) = fast_fading.fit_to_empirical_cdf(
        fast_fading_df["loss_db"].to_numpy(),
        results_path,
    )

    # Calculate and export the distribution's CDF
    fast_fading.export_cdf_to_csv(
        fast_fading_inverse_ecdf,
        paths.get_ml_model_path(dataset, mlpl_model, ml_algorithm),
    )

    # Save ML model, overwriting the file, if it already exists
    with open(_get_fast_fading_ml_model_path(dataset, mlpl_model, ml_algorithm), "wb") as f:
        pickle.dump((fast_fading_ecdf, fast_fading_inverse_ecdf), f)


def get_fast_fading_ml_model_predictor(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> Callable[[np.ndarray], np.ndarray]:
    """
    Get the fast-fading ML model prediction function.
    This function predicts the path loss (dB) based on a distance value (m) as input.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.

    Returns
    -------
        Callable function to predict the fast-fading.
    """

    print("Getting the fast-fading ML model")

    # Load ML model
    with open(_get_fast_fading_ml_model_path(dataset, mlpl_model, ml_algorithm), "rb") as f:
        (fast_fading_ecdf, fast_fading_inverse_ecdf) = pickle.load(f)

    # Create predictor
    def ff_predictor(ff_input: np.ndarray) -> np.ndarray:
        """
        Predict the fast-fading component for a given input.

        Args
        ----
            ff_input np.ndarray with the input to the fast-fading model.

        Returns
        -------
            np.ndarray with the fast-fading prediction.
        """

        ff_predictions = fast_fading.predict_fast_fading(
            fast_fading_inverse_ecdf,
            size=ff_input.shape[0],
        )

        ff_predictions = ff_predictions.squeeze()

        return ff_predictions

    return ff_predictor


#################################################
# PATHS
#################################################
def _get_path_loss_ml_model_path(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> str:
    """
    Get the path to the path loss ML model file.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.

    Returns
    -------
        Path to the path loss ML model.
    """

    return os.path.join(
        paths.get_ml_model_path(dataset, mlpl_model, ml_algorithm),
        "path-loss.pickle",
    )


def _get_fast_fading_ml_model_path(
    dataset: str,
    mlpl_model: MlplModel,
    ml_algorithm: MlAlgorithm,
) -> str:
    """
    Get the path to the fast-fading ML model file.

    Args
    ----
        dataset Dataset.
        mlpl_model MLPL model.
        ml_algorithm ML algorithm used to train the MLPL model.

    Returns
    -------
        Path to the fast-fading ML model.
    """

    return os.path.join(
        paths.get_ml_model_path(dataset, mlpl_model, ml_algorithm),
        "fast-fading-ecdf.pickle",
    )
