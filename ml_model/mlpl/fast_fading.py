# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Fast-fading models.
"""

import os
from typing import Tuple

import numpy as np
import pandas as pd
import scipy
import scipy.interpolate
import scipy.stats
from statsmodels.distributions.empirical_distribution import ECDF

from . import dataset_manager, results
from .mlpl_model import MlplModel


def calculate_fast_fading_from_propagation_loss(
    propagation_loss_df: pd.DataFrame,
    mlpl_model: MlplModel,
) -> pd.DataFrame:
    """
    Calculate the fast-fading based on the total propagation loss.
    The fast-fading loss for a distance is assumed to be the difference between the
    mean total propagation loss of that distance and the path loss of the distance.

    Args
    ----
        propagation_loss_df Total propagation loss DataFrame.
        mlpl_model MLPL model.

    Returns
    -------
        Fast-fading DataFrame.
    """

    df_columns = dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)

    # Add a column "loss_db_mean" with the mean value for each distance / position
    fast_fading_df = propagation_loss_df.join(
        propagation_loss_df.groupby(df_columns).mean(),
        on=df_columns,
        rsuffix="_mean",
    )

    # Update the column "loss_db" with the respective ratio
    fast_fading_df["loss_db"] -= fast_fading_df["loss_db_mean"]

    # Remove the column "loss_db_mean"
    fast_fading_df.drop(columns="loss_db_mean", inplace=True)

    return fast_fading_df


def predict_fast_fading(
    fast_fading_inverse_ecdf: scipy.interpolate.interp1d,
    size: int,
) -> np.ndarray:
    """
    Predict the fast-fading component for a set of distances (in m).

    Args
    ----
        distribution Fast-fading distribution name [from scipy.stats].
        distribution_parameters Distributions' parameters [from scipy.stats].
        size Number of fast-fading predictions.

    Returns
    -------
        np.ndarray with the fast-fading predictions (in dB).
    """

    cdf_ppf = np.random.uniform(0, 1, size=size)
    predictions = fast_fading_inverse_ecdf(cdf_ppf)

    return predictions


def fit_to_empirical_cdf(
    data: np.ndarray,
    results_path: str,
) -> Tuple[ECDF, scipy.interpolate.interp1d, float]:
    """
    Find the empirical CDF (ECDF) distribution that best fits the values, that is,
    the distribution that minimizes the Mean Squared Error (MSE) between the original values
    and their estimations using the fitted distribution.

    Args
    ----
        data Data to be fitted.
        results_path Path to the results directory.

    Returns
    -------
        Tuple: (ECDF [from statsmodels], Inverse ECDF, MSE).
    """

    print("Fitting fast-fading values to empirical CDF")

    # Fit data to distribution
    fast_fading_ecdf = ECDF(data)

    # Calculate the inverse ECDF
    slope_changes = sorted(set(data))
    sample_edf_values_at_slope_changes = [fast_fading_ecdf(item) for item in slope_changes]
    fast_fading_inverse_ecdf = scipy.interpolate.make_interp_spline(
        sample_edf_values_at_slope_changes,
        slope_changes,
        k=1,
    )

    # Calculate the mean squared error (MSE) for the fitted distribution
    BINS = 30

    x = np.linspace(np.min(data), np.max(data), BINS)

    data_histogram = scipy.stats.rv_histogram(np.histogram(data, bins=BINS))
    data_cdf_y = data_histogram.cdf(x)

    fast_fading_ecdf_y = fast_fading_ecdf(x)

    mse = np.mean((data_cdf_y - fast_fading_ecdf_y) ** 2, dtype=float)

    print(f"Fast-fading ECDF: MSE = {mse:.1e}")

    # Plot results
    results.plot_fast_fading_empirical_cdf(
        data,
        fast_fading_ecdf,
        mse,
        results_path,
    )

    return (fast_fading_ecdf, fast_fading_inverse_ecdf, mse)


def export_cdf_to_csv(
    fast_fading_inverse_ecdf: scipy.interpolate.interp1d,
    directory_path: str,
) -> None:
    """
    Export the fast-fading empirical CDF to a CSV file.
    The CSV file has two columns: 1) CDF percentile point (X) and 2) the CDF percentile rank (Y).

    Args
    ----
        fast_fading_inverse_ecdf Fast-fading inverse empirical CDF [from statsmodel].
        directory_path Path to the directory to save the CSV file.
    """

    print("Exporting fast-fading ECDF to CSV")

    # Generate (X, Y) pairs
    y = np.arange(0.01, 1.0, 0.01)
    x = fast_fading_inverse_ecdf(y)

    # Add pair (X, Y=1.0) (due to an ns-3 requirement)
    y = np.append(y, 1.0)
    x = np.append(x, x[-1])

    # Export CDF to CSV file
    cdf_df = pd.DataFrame({"x": x, "y": y})
    cdf_df.to_csv(
        os.path.join(directory_path, "fast-fading-ecdf.csv"),
        index=False,
        float_format="%.2e",
    )
