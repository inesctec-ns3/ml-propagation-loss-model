# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Wi-Fi parameters.
"""

import json
import os
from dataclasses import dataclass

from . import paths


@dataclass
class WifiParameters:
    """Wi-Fi parameters"""

    tx_power_dbm: float
    tx_gain_dbi: float
    rx_gain_dbi: float
    frequency_mhz: float
    bandwidth_mhz: float
    wifi_standard: str

    @classmethod
    def from_json(cls, dataset: str):
        """
        Create a new class by reading the corresponding wifi-parameters.json file.

        Args
        ----
            dataset Dataset.
        """

        wifi_parameters_path = os.path.join(paths.get_dataset_path(dataset), "wifi-parameters.json")

        with open(wifi_parameters_path, "r", encoding="utf-8") as f:
            parameters = json.load(f)

        return cls(**parameters)
