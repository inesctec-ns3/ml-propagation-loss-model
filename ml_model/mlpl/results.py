# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Results analysis.
"""

import itertools
import os
from typing import Callable, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
import scipy.stats
from statsmodels.distributions.empirical_distribution import ECDF

from . import dataset_manager
from .mlpl_model import MlplModel

# Parameters
plt.rcParams["pdf.fonttype"] = 42  # Set fonts to TrueType (embedded)
plt.rcParams["font.size"] = 12

_PLOT_MARKERS = ["o", "x", "s", "d", "^", "*", "v"]

_FIGURE_FORMATS = [
    "png",
    "pdf",
]


#################################################
# CALCULATE RESULTS
#################################################
def calculate_distance_from_positions(positions_df: pd.DataFrame) -> np.ndarray:
    """
    Calculate the Euclidean distance between the (x, y, z) positions of Tx and Rx nodes.

    Args
    ----
        positions_df Positions DataFrame.

    Returns
    -------
        Euclidean distances (in meters).
    """

    distances_m = np.sqrt(
        (
            (positions_df["x_tx"] - positions_df["x_rx"]) ** 2
            + (positions_df["y_tx"] - positions_df["y_rx"]) ** 2
            + (positions_df["z_tx"] - positions_df["z_rx"]) ** 2
        ).to_numpy()
    )

    return distances_m


def calculate_propagation_loss_error(
    propagation_loss_input_df: pd.DataFrame,
    real_losses_db: np.ndarray,
    predicted_losses_db: np.ndarray,
) -> pd.DataFrame:
    """
    Calculate the propagation loss error DataFrame to be used in the plots.

    Args
    ----
        propagation_loss_input_df Propagation loss input DataFrame (with only the columns related to the input).
        real_losses_db Real propagation losses (in dB).
        predicted_losses_db Predicted propagation losses (in dB).

    Returns
    -------
        DataFrame with the propagation loss error (in dB).
    """

    propagation_loss_error_df = propagation_loss_input_df.copy()
    propagation_loss_error_df["loss_error_db"] = predicted_losses_db - real_losses_db

    return propagation_loss_error_df


def calculate_ml_propagation_loss_error(
    loss_df: pd.DataFrame,
    loss_predictor: Callable[[np.ndarray], np.ndarray],
    mlpl_model: MlplModel,
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Calculate the propagation loss error DataFrame for the ML models,
    to be used in the plots.

    Args
    ----
        loss_df Propagation loss DataFrame.
        loss_predictor Callable for the propagation loss predictor.
        mlpl_model MLPL model.

    Returns
    -------
        Tuple [
            DataFrame with the propagation loss error (in dB),
            DataFrame with the propagation loss predictions (in dBm),
        ]
    """

    df_columns = dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)

    propagation_loss_input_df = loss_df[df_columns]
    loss_predictions = loss_predictor(propagation_loss_input_df.to_numpy())

    # Errors
    errors_df = calculate_propagation_loss_error(
        propagation_loss_input_df,
        loss_df["loss_db"].to_numpy(),
        loss_predictions,
    )

    # Predictions
    predictions_df = propagation_loss_input_df.copy()
    predictions_df["loss_db"] = loss_predictions

    return (errors_df, predictions_df)


def calculate_throughput_error(
    real_throughput_kbps_df: pd.DataFrame,
    throughput_kbps_df: pd.DataFrame,
    mlpl_model: MlplModel,
) -> pd.DataFrame:
    """
    Calculate the throughput error DataFrame, to be used in the plots.

    Args
    ----
        real_throughput_kbps_df Real throughput DataFrame (in kbit/s).
        throughput_kbps_df Custom throughput DataFrame (in kbit/s).
        mlpl_model MLPL model.

    Returns
    -------
        DataFrame with the throughput error (in kbit/s).
    """

    df_columns = dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)

    error_df = (
        throughput_kbps_df.set_index(df_columns) - real_throughput_kbps_df.set_index(df_columns)
    ).reset_index()
    error_df.rename(columns={"throughput_kbps": "throughput_error_kbps"}, inplace=True)

    return error_df


#################################################
# PLOTS
#################################################
def plot_fast_fading_empirical_cdf(
    data: np.ndarray,
    fast_fading_ecdf: ECDF,
    fast_fading_ecdf_mse: float,
    results_path: str,
) -> None:
    """
    Plot the distribution fitting of a given data with a list of statistical distributions.

    Args
    ----
        data Data to be fitted.
        fast_fading_ecdf The fast-fading empirical CDF [from statsmodel].
        fast_fading_ecdf_mse Fast-fading empirical CDF's Mean Squared Errors (MSE).
        results_path Path to the results directory.
    """

    BINS = 100
    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    plt.figure()

    # Calculate histogram of the original fast-fading values
    x = np.linspace(np.min(data), np.max(data), BINS)
    data_histogram = scipy.stats.rv_histogram(np.histogram(data, bins=BINS))

    plt.plot(
        x,
        data_histogram.cdf(x),
        marker=next(MARKERS_ITER),
        markevery=10,
        fillstyle="none",
        label="Real",
    )

    # Plot distribution fitting
    fast_fading_ecdf_y = fast_fading_ecdf(x)

    plt.plot(
        x,
        fast_fading_ecdf_y,
        marker=next(MARKERS_ITER),
        markevery=10,
        fillstyle="none",
        label=f"ECDF (MSE: {fast_fading_ecdf_mse:.1e})",
    )

    # Format figure and save it
    plt.title("Fast-Fading Empirical CDF")
    plt.xlabel("Fast-Fading Loss / dB")
    plt.ylabel("CDF")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f"fast-fading-ecdf.{fig_format}"),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_dataset_input_data(
    propagation_loss_df: pd.DataFrame,
    mlpl_model: MlplModel,
    results_path: str,
) -> None:
    """
    Plot the dataset input data.
    The plot type depends on the ML input type.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        mlpl_model MLPL model.
        results_path Path to the results directory.
    """

    if mlpl_model == MlplModel.DISTANCE:
        plot_dataset_distances_histogram(
            propagation_loss_df["distance_m"].to_numpy(),
            results_path,
        )
    elif mlpl_model == MlplModel.POSITION:
        plot_dataset_tx_rx_positions(
            propagation_loss_df,
            results_path,
        )
    else:
        raise ValueError(f"Invalid MLPL model: {mlpl_model}")


def plot_dataset_distances_histogram(
    distances: np.ndarray,
    results_path: str,
) -> None:
    """
    Plot a histogram of the dataset distances.

    Args
    ----
        distance Array of distances (in meter).
        results_path Path to the results directory.
    """

    plt.figure()
    plt.hist(distances)
    plt.title("Dataset Distances Histogram")
    plt.xlabel("Distance / m")
    plt.ylabel("Count")
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f"dataset-distances-histogram.{fig_format}"),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_dataset_tx_rx_positions(
    positions_df: pd.DataFrame,
    results_path: str,
) -> None:
    """
    Plot the (x, y, z)_tx and (x, y, z)_rx positions of the dataset.

    Args
    ----
        positions_df DataFrame with the (x, y, z)_tx and (x, y, z)_rx positions (in meter).
        results_path Path to the results directory.
    """

    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")

    def _plot_positions(tx_rx: str) -> None:
        """Plot Tx / Rx positions."""

        unique_positions_df = positions_df.drop_duplicates(
            subset=[f"{i}_{tx_rx}" for i in ["x", "y", "z"]]
        )

        ax.scatter(
            unique_positions_df[f"x_{tx_rx}"],
            unique_positions_df[f"y_{tx_rx}"],
            unique_positions_df[f"z_{tx_rx}"],
            label=f"{tx_rx.capitalize()} Positions",
            marker=next(MARKERS_ITER),
        )

    _plot_positions("tx")
    _plot_positions("rx")

    plt.title("Dataset Tx and Rx Positions")
    ax.set_xlabel("X / m")
    ax.set_ylabel("Y / m")
    ax.set_zlabel("Z / m")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f"dataset-tx-rx-positions.{fig_format}"),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_propagation_loss_distance_predictions(
    propagation_losses_df: List[pd.DataFrame],
    labels: List[str],
    results_path: str,
) -> None:
    """
    Plot the propagation loss predictions for a set of distances.

    Args
    ----
        propagation_losses_df List of DataFrames for the propagation losses to be plotted.
                              Each DataFrame has the distance and path loss (in dB).
        labels List of plot labels.
        results_path Path to the results directory.
    """

    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    plt.figure()

    for loss_df, label in zip(propagation_losses_df, labels):
        # Calculate the statistical values for each distance
        grouped_loss_df = loss_df.groupby("distance_m", as_index=False)

        # Plot the average value
        # avg_loss_df = grouped_loss_df.mean()
        avg_loss_df = grouped_loss_df.median()

        plt.plot(
            avg_loss_df["distance_m"],
            avg_loss_df["loss_db"],
            label=label,
            marker=next(MARKERS_ITER),
            fillstyle="none",
        )

        # Plot the median and percentiles 25, 75
        # median_loss_df = grouped_loss_df.median()
        # p25_loss_df = grouped_loss_df.quantile(0.25)
        # p75_loss_df = grouped_loss_df.quantile(0.75)
        #
        # color = next(COLORS_ITER)
        #
        # plt.plot(
        #     median_loss_df["distance_m"],
        #     median_loss_df["loss_db"],
        #     label=label,
        #     marker=next(MARKERS_ITER),
        #     fillstyle="none",
        #     color=color,
        #     linewidth=2,
        # )
        #
        # plt.fill_between(
        #     median_loss_df["distance_m"],
        #     p25_loss_df["loss_db"],
        #     p75_loss_df["loss_db"],
        #     marker=marker,
        #     markevery=1,
        #     fillstyle="none",
        #     color=color,
        #     alpha=0.1,
        # )

    plt.title("Propagation Loss Predictions")
    plt.xlabel("Distance / m")
    plt.ylabel("Median Loss / dB")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f"propagation-loss-predictions.{fig_format}"),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_propagation_loss_distance_errors(
    propagation_loss_errors_df: List[pd.DataFrame],
    labels: List[str],
    results_path: str,
) -> None:
    """
    Plot the propagation loss prediction errors over distances.

    Args
    ----
        propagation_loss_errors_df List of DataFrames for the propagation losses errors to be plotted.
                                   Each DataFrame has the distance and path loss error (in dB).
        labels List of plot labels.
        results_path Path to the results directory.
    """

    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    plt.figure()

    for loss_df, label in zip(propagation_loss_errors_df, labels):
        # Calculate the statistical values for each input
        grouped_loss_df = loss_df.groupby("distance_m", as_index=False)

        # Plot the average value
        # avg_loss_df = grouped_loss_df.mean()
        avg_loss_df = grouped_loss_df.median()

        plt.plot(
            avg_loss_df["distance_m"],
            avg_loss_df["loss_error_db"],
            label=label,
            marker=next(MARKERS_ITER),
            fillstyle="none",
        )

        # Plot the median and percentiles 25, 75
        # median_loss_df = grouped_loss_df.median()
        # p25_loss_df = grouped_loss_df.quantile(0.25)
        # p75_loss_df = grouped_loss_df.quantile(0.75)
        #
        # color = next(COLORS_ITER)
        #
        # plt.plot(
        #     median_loss_df["distance_m"],
        #     median_loss_df["loss_error_db"],
        #     label=label,
        #     marker=next(MARKERS_ITER),
        #     fillstyle="none",
        #     color=color,
        #     linewidth=2,
        # )
        #
        # plt.fill_between(
        #     median_loss_df["distance_m"],
        #     p25_loss_df["loss_error_db"],
        #     p75_loss_df["loss_error_db"],
        #     marker=marker,
        #     markevery=1,
        #     fillstyle="none",
        #     color=color,
        #     alpha=0.1,
        # )

    plt.title("Propagation Loss Prediction Error")
    plt.xlabel("Distance / m")
    plt.ylabel("Median Loss Error / dB")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f"propagation-loss-prediction-error.{fig_format}"),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_propagation_loss_errors_cdf(
    propagation_loss_errors_df: List[pd.DataFrame],
    labels: List[str],
    plot_absolute_errors: bool,
    results_path: str,
) -> None:
    """
    Plot the propagation loss prediction errors CDF.

    Args
    ----
        propagation_loss_errors_df List of DataFrames for the propagation loss errors to be plotted.
                                   Each DataFrame has the distance and path loss error (in dB).
        labels List of plot labels.
        plot_absolute_errors True: plot absolute (modulo) errors | False: plot relative errors.
        results_path Path to the results directory.
    """

    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    plt.figure()

    for loss_df, label in zip(propagation_loss_errors_df, labels):
        error = loss_df["loss_error_db"].to_numpy()

        if plot_absolute_errors:
            error = np.abs(error)

        count, bins_count = np.histogram(error, bins=201)
        cdf = np.cumsum(count / np.sum(count))

        plt.plot(
            bins_count[1:],
            cdf,
            label=label,
            marker=next(MARKERS_ITER),
            markevery=10,
            fillstyle="none",
        )

    if plot_absolute_errors:
        plot_title = "Propagation Loss Absolute Prediction Error CDF"
    else:
        plot_title = "Propagation Loss Prediction Error CDF"

    plt.title(plot_title)
    plt.xlabel("Loss Error / dB")
    plt.ylabel("CDF")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f'{plot_title.lower().replace(" ", "-")}.{fig_format}'),
            bbox_inches="tight",
            format=fig_format,
        )


def plot_throughput_errors_cdf(
    throughput_errors_df: List[pd.DataFrame],
    labels: List[str],
    plot_absolute_errors: bool,
    results_path: str,
) -> None:
    """
    Plot the throughput errors CDF.

    Args
    ----
        throughput_errors_df List of DataFrames for the throughput errors to be plotted.
                             Each DataFrame has the distance / position and the throughput error (in kbit/s).
        labels List of plot labels.
        plot_absolute_errors True: plot absolute (modulo) errors | False: plot relative errors.
        results_path Path to the results directory.
    """

    MARKERS_ITER = itertools.cycle(_PLOT_MARKERS)

    plt.figure()

    for throughput_df, label in zip(throughput_errors_df, labels):
        error = throughput_df["throughput_error_kbps"].to_numpy()

        if plot_absolute_errors:
            error = np.abs(error)

        count, bins_count = np.histogram(error, bins=201)
        cdf = np.cumsum(count / np.sum(count))

        plt.plot(
            bins_count[1:],
            cdf,
            label=label,
            marker=next(MARKERS_ITER),
            markevery=10,
            fillstyle="none",
        )

    if plot_absolute_errors:
        plot_title = "Average Absolute Throughput Error CDF"
    else:
        plot_title = "Average Throughput Error CDF"

    plt.title(plot_title)
    plt.xlabel("Throughput Error / kbit/s")
    plt.ylabel("CDF")
    plt.legend()
    plt.grid()

    for fig_format in _FIGURE_FORMATS:
        plt.savefig(
            os.path.join(results_path, f'{plot_title.lower().replace(" ", "-")}.{fig_format}'),
            bbox_inches="tight",
            format=fig_format,
        )
