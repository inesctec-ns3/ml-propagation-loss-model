# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.

"""
Path loss models.
"""

from typing import List, Tuple

import numpy as np
import pandas as pd

from . import dataset_manager, results
from .mlpl_model import MlplModel
from .wifi_parameters import WifiParameters


def calculate_path_loss_from_propagation_loss(
    propagation_loss_df: pd.DataFrame,
    mlpl_model: MlplModel,
) -> pd.DataFrame:
    """
    Calculate the path loss based on the total propagation loss.
    The path loss for a distance is assumed to be the average loss of that distance / position.

    Args
    ----
        propagation_loss_df Total propagation loss DataFrame.
        mlpl_model MLPL model.

    Returns
    -------
        Path loss DataFrame.
    """

    df_columns = dataset_manager.get_propagation_loss_df_input_column_names(mlpl_model)
    path_loss_df = propagation_loss_df.groupby(df_columns, as_index=False).mean().round()

    return path_loss_df


def calculate_baseline_path_losses(
    path_loss_input_df: pd.DataFrame,
    wifiParameters: WifiParameters,
) -> Tuple[List[pd.DataFrame], List[str]]:
    """
    Calculate the path loss for a set of baselines.

    Args
    ----
        path_loss_input_df Path loss input DataFrame (with only the columns related to the input).
        wifiParameters Wi-Fi parameters.

    Returns
    -------
        Tuple [
            List of DataFrames with the baseline path losses,
            List of the baseline labels
        ]
    """

    def _create_loss_df(losses: np.ndarray) -> pd.DataFrame:
        """Create a loss DataFrame."""

        loss_df = path_loss_input_df.copy()
        loss_df["loss_db"] = losses

        return loss_df

    path_losses_df: List[pd.DataFrame] = []
    labels: List[str] = []

    if "distance_m" in path_loss_input_df:
        distances_m = path_loss_input_df["distance_m"].to_numpy()
    else:
        distances_m = results.calculate_distance_from_positions(path_loss_input_df)

    # Calculate the fading loss to add to the baselines
    fading_loss = _calculate_fading_loss(distances_m)

    # Friis
    labels.append("Friis")

    friis_loss = _calculate_friis_loss(
        distances_m,
        wifiParameters.frequency_mhz,
    )

    path_losses_df.append(_create_loss_df(friis_loss + fading_loss))

    # Log-distance
    LOG_DISTANCE_GAMMA = 1.7

    labels.append(f"LogDist-{LOG_DISTANCE_GAMMA}")

    log_dist_loss = _calculate_log_distance_loss(
        distances_m,
        wifiParameters.frequency_mhz,
        LOG_DISTANCE_GAMMA,
    )

    path_losses_df.append(_create_loss_df(log_dist_loss + fading_loss))

    return (path_losses_df, labels)


def _calculate_friis_loss(
    distances_m: np.ndarray | float,
    frequency_mhz: float,
) -> np.ndarray:
    """
    Calculate the Friis loss for a set of distances.

    Args
    ----
        distances_m Distances (in m).
        frequency_mhz Frequency (in MHz).

    Returns
    -------
        Losses (> 0) (in dB).
    """

    if np.any(distances_m <= 0):
        raise ValueError("Distances must be > 0")

    C = 3e8

    losses_db = -20 * np.log10(C / (4 * np.pi * frequency_mhz * 1e6 * distances_m))

    return losses_db


def _calculate_log_distance_loss(
    distances_m: np.ndarray | float,
    frequency_mhz: float,
    gamma: float,
) -> np.ndarray:
    """
    Calculate the Log-distance loss for a set of distances.

    Args
    ----
        distances_m Distances (in m).
        frequency_mhz Frequency (in MHz).
        gamma Path loss exponent (gamma).

    Returns
    -------
        Losses (> 0) (in dB).
    """

    if np.any(distances_m <= 0):
        raise ValueError("Distances must be > 0")

    # Path loss for reference distance
    d0 = 1
    pl0 = _calculate_friis_loss(d0, frequency_mhz)

    # Log-distance path loss
    losses_db = pl0 + 10 * gamma * np.log10(distances_m / d0)

    return losses_db


def _calculate_fading_loss(distances: np.ndarray) -> np.ndarray:
    """
    Calculate the fading loss for a set of distances, due to shadowing and fast-fading.
    The fading is modeled as a Normal distribution.

    Args
    ----
        distances Distances (in m).

    Returns
    -------
        Losses (> 0) (in dB).
    """

    MEAN = 0
    STD = 3

    loss_db = np.random.normal(MEAN, STD, size=distances.shape)

    return loss_db
