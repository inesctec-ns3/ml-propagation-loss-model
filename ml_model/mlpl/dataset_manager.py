# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
Dataset manager.
Contains functions to load and process the dataset.
"""

import os
from typing import List, Tuple

import numpy as np
import pandas as pd
import scipy
import scipy.stats
import sklearn
import sklearn.model_selection

from . import fast_fading, path_loss, paths, results
from .mlpl_model import MlplModel
from .wifi_parameters import WifiParameters


def get_propagation_loss_df_input_column_names(mlpl_model: MlplModel) -> List[str]:
    """
    Get the DataFrame column names related to the input of the ML propagation loss model.

    Args
    ----
        mlpl_model MLPL model.

    Returns
    -------
        List of strings with the DataFrame column names.
    """

    if mlpl_model == MlplModel.DISTANCE:
        df_columns = ["distance_m"]
    elif mlpl_model == MlplModel.POSITION:
        df_columns = ["x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx"]
    else:
        raise ValueError(f"Invalid MLPL model: {mlpl_model}")

    return df_columns


def load_propagation_loss_dataset(
    dataset: str,
    mlpl_model: MlplModel,
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Load the propagation loss dataset CSV file.
    If necessary, convert raw parameters in the dataset to the simple format.

    Args
    ----
        dataset Dataset name.
        mlpl_model MLPL model.

    Returns
    -------
        Tuple [Propagation loss DataFrame, Path loss DataFrame, Fast-fading DataFrame].
    """

    path = os.path.join(paths.get_dataset_path(dataset), "propagation-loss-dataset.csv")
    propagation_loss_df = pd.read_csv(path, dtype=np.float32)

    propagation_loss_df = _check_update_dataset_columns(propagation_loss_df, dataset, mlpl_model)
    propagation_loss_df = _preprocess_propagation_loss_df(propagation_loss_df)

    # Path loss
    path_loss_df = path_loss.calculate_path_loss_from_propagation_loss(
        propagation_loss_df,
        mlpl_model,
    )

    # Fast-fading
    fast_fading_df = fast_fading.calculate_fast_fading_from_propagation_loss(
        propagation_loss_df,
        mlpl_model,
    )

    return (propagation_loss_df, path_loss_df, fast_fading_df)


def split_dataset_into_train_test(
    propagation_loss_df: pd.DataFrame,
    path_loss_df: pd.DataFrame,
    fast_fading_df: pd.DataFrame,
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Split the dataset into a training and test sets.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        path_loss_df Path loss DataFrame.
        fast_fading_df Fast-fading DataFrame.

    Returns
    -------
        Tuple [
            Propagation loss train DataFrame, Path loss train DataFrame, Fast-fading train DataFrame,
            Propagation loss test DataFrame, Path loss test DataFrame, Fast-fading test DataFrame,
        ].
    """

    TEST_SIZE = 0.2
    RANDOM_STATE = 1

    (
        propagation_loss_train_df,
        propagation_loss_test_df,
    ) = sklearn.model_selection.train_test_split(
        propagation_loss_df,
        test_size=TEST_SIZE,
        random_state=RANDOM_STATE,
    )

    (path_loss_train_df, path_loss_test_df) = sklearn.model_selection.train_test_split(
        path_loss_df,
        test_size=TEST_SIZE,
        random_state=RANDOM_STATE,
    )

    (fast_fading_train_df, fast_fading_test_df) = sklearn.model_selection.train_test_split(
        fast_fading_df,
        test_size=TEST_SIZE,
        random_state=RANDOM_STATE,
    )

    return (
        propagation_loss_train_df,
        propagation_loss_test_df,
        path_loss_train_df,
        path_loss_test_df,
        fast_fading_train_df,
        fast_fading_test_df,
    )


def create_unique_positions_dataset(dataset: str, mlpl_model: MlplModel) -> None:
    """
    Create a dataset with unique Tx / Rx positions.
    The remaining columns correspond to the average value for the same positions.

    Args
    ----
        dataset Dataset name.
        mlpl_model MLPL model.
    """

    original_dataset_path = os.path.join(
        paths.get_dataset_path(dataset),
        "propagation-loss-dataset.csv",
    )

    unique_dataset_path = os.path.join(
        paths.get_unique_dataset_path(dataset),
        "propagation-loss-unique-dataset.csv",
    )

    # Read the original dataset
    dataset_df = pd.read_csv(original_dataset_path, dtype=np.float32)

    # Remove the outliers
    dataset_df = dataset_df.round()

    if "loss_db" in dataset_df:
        dataset_df = _remove_outliers(dataset_df, "loss_db")
    elif "snr_db" in dataset_df:
        dataset_df = _remove_outliers(dataset_df, "snr_db")

    dataset_df = _remove_outliers(dataset_df, "throughput_kbps")

    # Calculate the unique position values
    position_df_columns = get_propagation_loss_df_input_column_names(mlpl_model)
    dataset_df = dataset_df.groupby(position_df_columns, as_index=False).mean().round(1)

    # Save the dataset
    dataset_df.to_csv(unique_dataset_path, index=False)


def _check_update_dataset_columns(
    propagation_loss_df: pd.DataFrame,
    dataset: str,
    mlpl_model: MlplModel,
) -> pd.DataFrame:
    """
    Check if the propagation loss dataset has the required columns.
    Convert raw values to the simple columns.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        dataset Dataset name.
        mlpl_model MLPL model.

    Returns
    -------
        Updated propagation loss DataFrame.
    """

    # Loss
    if "loss_db" not in propagation_loss_df:
        if {"snr_db", "noise_dbm"}.issubset(propagation_loss_df.columns):
            wifi_parameters = WifiParameters.from_json(dataset)

            propagation_loss_df = _convert_snr_to_loss(
                propagation_loss_df,
                wifi_parameters,
            )
        elif "rx_power_dbm" in propagation_loss_df:
            wifi_parameters = WifiParameters.from_json(dataset)

            propagation_loss_df = _convert_rx_power_to_loss(
                propagation_loss_df,
                wifi_parameters,
            )
        else:
            raise ValueError('Dataset does not have columns "loss_db", "snr_db" nor "noise_dbm"')

    # Distance / position
    if mlpl_model == MlplModel.DISTANCE:
        if "distance_m" not in propagation_loss_df:
            if {"x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx"}.issubset(
                propagation_loss_df.columns
            ):
                propagation_loss_df = _convert_tx_rx_positions_to_distance(propagation_loss_df)
            else:
                raise ValueError(
                    f'Dataset loaded with {mlpl_model=}, but does not have columns "distance_m", nor ("x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx")'
                )

    elif mlpl_model == MlplModel.POSITION:
        if not {"x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx"}.issubset(
            propagation_loss_df.columns
        ):
            raise ValueError(
                f'Dataset loaded with {mlpl_model=}, but does not have columns ("x_tx", "y_tx", "z_tx", "x_rx", "y_rx", "z_rx")'
            )

    else:
        raise ValueError(f"Invalid MLPL model: {mlpl_model}")

    return propagation_loss_df


def _convert_snr_to_loss(
    propagation_loss_df: pd.DataFrame,
    wifi_parameters: WifiParameters,
) -> pd.DataFrame:
    """
    Convert the SNR and noise values to propagation loss.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        wifi_parameters Wi-Fi parameters.

    Returns
    -------
        Updated DataFrame with the propagation loss.
    """

    propagation_loss_df["loss_db"] = (
        wifi_parameters.tx_power_dbm
        + wifi_parameters.tx_gain_dbi
        + wifi_parameters.rx_gain_dbi
        - propagation_loss_df["snr_db"]
        - propagation_loss_df["noise_dbm"]
    ).round()

    return propagation_loss_df


def _convert_rx_power_to_loss(
    propagation_loss_df: pd.DataFrame,
    wifi_parameters: WifiParameters,
) -> pd.DataFrame:
    """
    Convert the RX power to propagation loss.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        wifi_parameters Wi-Fi parameters.

    Returns
    -------
        Updated DataFrame with the propagation loss.
    """

    propagation_loss_df["loss_db"] = (
        propagation_loss_df["rx_power_dbm"]
        - wifi_parameters.tx_power_dbm
        - wifi_parameters.tx_gain_dbi
        - wifi_parameters.rx_gain_dbi
    ).round()

    return propagation_loss_df


def _convert_tx_rx_positions_to_distance(propagation_loss_df: pd.DataFrame) -> pd.DataFrame:
    """
    Convert the (x, y, z) positions of Tx and Rx nodes to the Euclidean distance.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.

    Returns
    -------
        Updated DataFrame with the distance.
    """

    propagation_loss_df["distance_m"] = results.calculate_distance_from_positions(
        propagation_loss_df
    ).round()

    return propagation_loss_df


def _preprocess_propagation_loss_df(propagation_loss_df: pd.DataFrame) -> pd.DataFrame:
    """
    Pre-process a loss DataFrame (e.g., path loss, fast-fading, propagation loss).

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.

    Returns
    -------
        Processed propagation loss DataFrame.
    """

    propagation_loss_df = propagation_loss_df.round()
    propagation_loss_df = _remove_outliers(propagation_loss_df, "loss_db")

    return propagation_loss_df


def _remove_outliers(propagation_loss_df: pd.DataFrame, column: str) -> pd.DataFrame:
    """
    Remove the outliers from the dataset, considering the values in the given column.
    The outliers are the values which are much higher or lower than the average.

    Args
    ----
        propagation_loss_df Propagation loss DataFrame.
        column DataFrame column on which to consider the outliers.

    Returns
    -------
        Updated propagation loss DataFrame without the outliers.
    """

    Z_SCORE_ABS_LIMIT = 5

    z_scores_abs = np.abs(scipy.stats.zscore(propagation_loss_df[column]))
    propagation_loss_df = propagation_loss_df[z_scores_abs < Z_SCORE_ABS_LIMIT]
    propagation_loss_df.reset_index(inplace=True, drop=True)

    return propagation_loss_df
