# Copyright (C) 2020-2025, INESC TEC.
# Machine Learning Based Propagation Loss Model for ns-3.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the European Union Public Licence v1.2 as published by the European Union.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the European Union Public Licence v1.2 for more details.
#
# You should have received a copy of the European Union Public Licence v1.2 along with this
# program. If not, see <https://www.eupl.eu>.
#
# Authors:
#   Eduardo Nuno Almeida [INESC TEC, Portugal]

"""
ML training algorithm enum.
"""

from enum import Enum


class MlAlgorithm(Enum):
    """ML training algorithm enum."""

    XGB = "xgb"
    SVR = "svr"

    def __str__(self) -> str:
        """String representation."""

        return self.value

    def __repr__(self) -> str:
        """Representation."""

        return self.value
