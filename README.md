# Machine Learning based Propagation Loss (MLPL) Model for the Network Simulator 3 (ns-3)

The Machine Learning based Propagation Loss (MLPL) model is a module for the [network simulator 3 (ns-3)](https://www.nsnam.org) that uses machine learning to train a propagation loss model to reproduce the physical conditions of an experimental testbed. The propagation loss model is trained with network traces collected in the experimental testbed that shall be reproduced.

Copyright (C) 2020-2025, INESC TEC. This project is licensed under the terms of the [European Union Public Licence (EUPL) v1.2](LICENSE)

## Authors

This project is authored by:

* Eduardo Nuno Almeida \[INESC TEC, Portugal\] \[eduardo.n.almeida (at) inesctec.pt ; enmsa (at) outlook.pt\]
* Mohammed Rushad, Sumanth Reddy Kota, Akshat Nambiar, Hardik L. Harti, Chinmay Gupta, Danish Waseem, Mohit P. Tahiliani \[NITK Surathkal, India\]
* Gonçalo Santos, Helder Fontes, Rui Campos, Manuel Ricardo \[INESC TEC, Portugal\]

## Releases and ns-3 App Store

All stable releases of the MLPL module are available in the [GitLab Releases](https://gitlab.com/inesctec-ns3/ml-propagation-loss-model/-/releases) page.

Additionally, the MLPL module is listed in the official [ns-3 App Store](https://apps.nsnam.org/app/mlpl/).

## ML Propagation Loss Models

The MLPL module contains two models depending on the method to train and predict the propagation loss.

* D-MLPL: Distance-based ML Propagation Loss Model
  * Train and predict the propagation loss according to the distance between the nodes.
* P-MLPL: Position-based ML Propagation Loss Model
  * Train and predict the propagation loss according to the positions of the nodes.

They are both implemented in the same set of files. The term MLPL is used to refer the generic model, regardless of the specific sub-model used. The specific sub-models are referred by their acronyms.

## Project Structure

The project is structured as follows:

* [datasets/](datasets/): Datasets to train the MLPL model
* [doc/](doc/): Model's documentation
* [examples/](examples/): MLPL's ns-3 examples
* [ml_model/](ml_model/): Scripts to train and run the MLPL's ML models
* [model/](model/): MLPL's ns-3 model
* [test/](test/): MLPL's ns-3 test suite

## MLPL Setup

The instructions to set-up and run this project are explained below.

### Setup ns-3 and Dependencies

1. Download the latest version of [ns-3](https://gitlab.com/nsnam/ns-3-dev).

  ```shell
  git clone https://gitlab.com/nsnam/ns-3-dev.git
  ```

2. Download the [ns3-ai](https://github.com/hust-diangroup/ns3-ai) app from the [GitHub repository](https://github.com/hust-diangroup/ns3-ai) or the [ns-3 App Store](https://apps.nsnam.org/app/ns3-ai/) and install it in the `contrib/` subdirectory. Refer to the installation instructions available on the GitHub repository.

  **NOTE**: The MLPL module is currently incompatible with the latest version of ns3-ai. Please use ns3-ai v1.2.0, while the issues are being fixed.

  ```shell
  cd ns-3-dev/contrib/
  git clone --branch=v1.2.0 https://github.com/hust-diangroup/ns3-ai.git
  ```

3. Clone this repository into the `contrib/` folder.

  ```shell
  cd ns-3-dev/contrib
  git clone https://gitlab.com/inesctec-ns3/ml-propagation-loss-model.git
  ```

### Setup Python

As a best practice, it is recommended to create a Conda / PyEnv virtual environment for this project. This creates a separate and isolated Python environment where all dependencies can be installed without interfering with other existing environments (including the base environment).

To create a virtual environment, run the following commands:

```shell
python3 -m venv venv/
```

To activate the new virtual environment, run the following command:

```shell
source venv/bin/activate
```

The Python dependencies are registered in the [requirements.txt](requirements.txt) file. To install them, run the following command:

```shell
pip install -r requirements.txt
```

### Build the ns-3 ML Propagation Loss Model

To build the ns-3 ML Propagation Loss model, run the following commands:

```shell
./ns3 configure --enable-examples --enable-tests
./ns3 build
```

## MLPL Model Training and Usage

The MLPL model training and usage in ns-3 are explained below.

### Datasets

The datasets used to train the MLPL model are stored in the [datasets](datasets) directory. The documentation about the datasets is available in the following page:

[MLPL Dataset Documentation](doc/datasets.md)

### ML Model Training

The scripts to train the ML models are located in the [ml_model/](ml_model/) directory. The documentation about training and running the ML models supporting the MLPL model are available in the following page:

[ML Model Training](doc/ml-model-training.md)

**NOTE**: The examples and tests requires the ML models to be trained beforehand. To train the ML models, run the following command, as explained in [ML Model Training](doc/ml-model-training.md):

```shell
python ml_model/train_ml_propagation_loss_model.py \
  --dataset=DATASET \
  --mlpl-model=MLPL_MODEL \
  --ml-algorithm=ML_ALGORITHM
```

### MLPL Model Usage in ns-3

The documentation about using the MLPL model in ns-3 is available in the following page:

[MLPL Usage in ns-3](doc/mlpl-usage-ns3.md).

### MLPL Tests

This module contains tests that check that the module is working as expected. The tests are located in the directory `tests/`.

To run the tests, first open a terminal to run the ML models, as explained in [MLPL Usage in ns-3](doc/mlpl-usage-ns3.md).

```shell
python ml_model/run_ml_propagation_loss_model.py \
  --dataset=position-dataset-example \
  --mlpl-model=position \
  --ml-algorithm=xgb
```

Then, run the tests in another terminal with the following command:

```shell
./test.py -s "ml-propagation-loss-model"
```

**NOTE**: Currently, the test suite only works with the parameters stated in the command above.

### MLPL Examples

The example `examples/ml-propagation-loss-throughput.cc` calculates the expected throughput between two nodes using the MLPL model for calculating the propagation loss.

To run the example, first open a terminal to activate the ML models, as explained [MLPL Usage in ns-3](doc/mlpl-usage-ns3.md).

```shell
python ml_model/run_ml_propagation_loss_model.py \
  --dataset=DATASET \
  --mlpl-model=MLPL_MODEL \
  --ml-algorithm=ML_ALGORITHM
```

Then, run the example in another terminal with the following command:

```shell
./ns3 run "ml-propagation-loss-model-throughput --dataset=DATASET --lossModel=LOSS_MODEL"
```

The arguments of the scripts correspond to the ML model that shall be run and allow the selection of the dataset, the MLPL model and the ML algorithm.

## Publications

The list of publications in international conferences and journals about the MLPL model is available in the following page:

[List of Publications](doc/publications.md)

## Cite This Module

If you would like to use this module, please cite it as follows:

Eduardo Nuno Almeida, Helder Fontes, Rui Campos, and Manuel Ricardo. 2023. Position-Based Machine Learning Propagation Loss Model Enabling Fast Digital Twins of Wireless Networks in ns-3. In Proceedings of the 2023 Workshop on ns-3 (WNS3 '23). ACM, 69–77. <https://doi.org/10.1145/3592149.3592150>

📊 [Presentation Slides](https://drive.inesctec.pt/s/YQ5x8enaWNJT8rj)
🎞️ [Presentation Video](https://drive.inesctec.pt/s/FrBmzPWf7wKWE7Q)

🏆 Best Paper Award
🏆 ACM Artifacts Available Badge

<details>
<summary>BibTex</summary>

```bibtex
@inproceedings{almeida2023position,
  author = {Almeida, Eduardo Nuno and Fontes, Helder and Campos, Rui and Ricardo, Manuel},
  title = {Position-Based Machine Learning Propagation Loss Model Enabling Fast Digital Twins of Wireless Networks in ns-3},
  year = {2023},
  isbn = {9798400707476},
  publisher = {ACM},
  booktitle = {Proceedings of the 2023 Workshop on ns-3},
  pages = {69–-77},
  doi = {10.1145/3592149.3592150},
}
```

</details>

## Bug Reports

Bug reports or feature suggestions can be made by creating an issue in the [issue tracker](https://gitlab.com/inesctec-ns3/ml-propagation-loss-model/-/issues).

## Contributions

Contributions to this module are welcome. If you would like to contribute to this module, feel free to open a [merge request](https://gitlab.com/inesctec-ns3/ml-propagation-loss-model/-/merge_requests).

Contributions to this module are subject to the [license](LICENSE) terms defined in this repository.
