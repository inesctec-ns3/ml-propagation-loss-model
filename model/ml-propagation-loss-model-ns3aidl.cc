// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.

#include "ml-propagation-loss-model-ns3aidl.h"

#include "ns3/vector.h"

#include <tuple>

namespace ns3
{

MlPropagationLossModelNs3AIDL::MlPropagationLossModelNs3AIDL(uint16_t id)
    : Ns3AIDL<MlFeature, MlPredicted, MlTarget>(id)
{
    SetCond(2, 0);
}

MlPropagationLossModelNs3AIDL::~MlPropagationLossModelNs3AIDL()
{
}

void
MlPropagationLossModelNs3AIDL::SetNodePositions(const Vector& txPosition, const Vector& rxPosition)
{
    MlFeature* featureSetterCond = FeatureSetterCond();

    featureSetterCond->xTx = txPosition.x;
    featureSetterCond->yTx = txPosition.y;
    featureSetterCond->zTx = txPosition.z;

    featureSetterCond->xRx = rxPosition.x;
    featureSetterCond->yRx = rxPosition.y;
    featureSetterCond->zRx = rxPosition.z;

    SetCompleted();
}

std::tuple<Vector, Vector>
MlPropagationLossModelNs3AIDL::GetNodePositions()
{
    MlFeature* featureGetterCond = FeatureGetterCond();

    double xTx = featureGetterCond->xTx;
    double yTx = featureGetterCond->yTx;
    double zTx = featureGetterCond->zTx;

    double xRx = featureGetterCond->xRx;
    double yRx = featureGetterCond->yRx;
    double zRx = featureGetterCond->zRx;

    GetCompleted();

    Vector txPosition(xTx, yTx, zTx);
    Vector rxPosition(xRx, yRx, zRx);

    return {txPosition, rxPosition};
}

void
MlPropagationLossModelNs3AIDL::SetPredictedPathLossDb(double pathLossDb)
{
    MlPredicted* predictedSetterCond = PredictedSetterCond();

    predictedSetterCond->pathLossDb = pathLossDb;

    SetCompleted();
}

double
MlPropagationLossModelNs3AIDL::GetPredictedPathLossDb()
{
    MlPredicted* predictedSetterCond = PredictedGetterCond();

    double pathLossDb = predictedSetterCond->pathLossDb;

    GetCompleted();

    return pathLossDb;
}

void
MlPropagationLossModelNs3AIDL::SetTargetPathLossDb(double pathLossDb)
{
    MlTarget* targetSetterCond = TargetSetterCond();

    targetSetterCond->pathLossDb = pathLossDb;

    SetCompleted();
}

double
MlPropagationLossModelNs3AIDL::GetTargetPathLossDb()
{
    MlTarget* targetGetterCond = TargetGetterCond();

    double pathLossDb = targetGetterCond->pathLossDb;

    GetCompleted();

    return pathLossDb;
}

} // namespace ns3
