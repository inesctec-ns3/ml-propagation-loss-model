// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.

#pragma once

#include "ns3/ns3-ai-dl.h"
#include "ns3/vector.h"

#include <tuple>

namespace ns3
{

/**
 * Structure storing the input features of the ML model,
 * according to ns3-ai-dl format.
 */
struct MlFeature
{
    double xTx; //!< X coordinates of the Tx node (in m)
    double yTx; //!< Y coordinates of the Tx node (in m)
    double zTx; //!< Z coordinates of the Tx node (in m)

    double xRx; //!< X coordinates of the Rx node (in m)
    double yRx; //!< Y coordinates of the Rx node (in m)
    double zRx; //!< Z coordinates of the Rx node (in m)
};

/**
 * Structure storing the output predictions of the ML model,
 * according to ns3-ai-dl format.
 */
struct MlPredicted
{
    double pathLossDb; //!< Predicted path loss (> 0) for the Tx / Rx positions (in dB)
};

/**
 * Structure storing the output targets (real values) of the ML model,
 * according to ns3-ai-dl format.
 */
struct MlTarget
{
    double pathLossDb; //!< Target (real) path loss (> 0) for the Tx / Rx positions (in dB)
};

/**
 * ML propagation loss model Ns3AIDL.
 */
class MlPropagationLossModelNs3AIDL : public Ns3AIDL<MlFeature, MlPredicted, MlTarget>
{
  public:
    /**
     * Constructor.
     *
     * @param id Shared memory ID used by ns3-ai.
     *           Must match MEMBLOCK_KEY in Python.
     */
    MlPropagationLossModelNs3AIDL(uint16_t id);

    // Delete default constructor to avoid misuse
    MlPropagationLossModelNs3AIDL() = delete;

    /**
     * Destructor.
     */
    ~MlPropagationLossModelNs3AIDL();

    /**
     * Set the Tx and Rx node positions, stored in the ns3-ai structure.
     *
     * @param txPosition Tx node position vector (in m).
     * @param rxPosition Rx node position vector (in m).
     */
    void SetNodePositions(const Vector& txPosition, const Vector& rxPosition);

    /**
     * Get the Tx and Rx node positions, stored in the ns3-ai structure.
     *
     * @return Tuple: [Tx position vector (in m), Rx position vector (in m)].
     */
    std::tuple<Vector, Vector> GetNodePositions();

    /**
     * Set the path loss predicted (> 0) by the ML model, stored in the ns3-ai structure.
     *
     * @param pathLossDb Predicted path loss (> 0) (in dB).
     */
    void SetPredictedPathLossDb(double pathLossDb);

    /**
     * Get the path loss value predicted (> 0) by the ML model, stored in the ns3-ai structure.
     *
     * @return Predicted path loss (> 0) (in dB).
     */
    double GetPredictedPathLossDb();

    /**
     * Set the path loss target (real value) (> 0) to be used by the ML model, stored in the ns3-ai
     * structure.
     *
     * @param pathLossDb Target path loss (real value) (> 0) (in dB).
     */
    void SetTargetPathLossDb(double pathLossDb);

    /**
     * Get the path loss target (real value) to be used by the ML model, stored in the ns3-ai
     * structure.
     *
     * @return Target path loss (real value) (> 0) (in dB).
     */
    double GetTargetPathLossDb();
};

} // namespace ns3
