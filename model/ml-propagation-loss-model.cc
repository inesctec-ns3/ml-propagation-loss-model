// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.

#include "ml-propagation-loss-model.h"

#include "ns3/boolean.h"
#include "ns3/csv-reader.h"
#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/random-variable-stream.h"
#include "ns3/string.h"
#include "ns3/vector.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE("MlPropagationLossModel");
NS_OBJECT_ENSURE_REGISTERED(MlPropagationLossModel);

/// Shared memory ID used by ns3-ai. Must match MEMBLOCK_KEY in Python.
constexpr uint32_t NS3_AIDL_ID = 3005;

TypeId
MlPropagationLossModel::GetTypeId()
{
    static TypeId tid =
        TypeId("ns3::MlPropagationLossModel")
            .SetParent<PropagationLossModel>()
            .SetGroupName("Propagation")
            .AddConstructor<MlPropagationLossModel>()
            .AddAttribute("FastFadingCdfPath",
                          "Absolute path to the CSV file with the fast-fading CDF model",
                          StringValue(""),
                          MakeStringAccessor(&MlPropagationLossModel::SetFastFadingCdfPath,
                                             &MlPropagationLossModel::GetFastFadingCdfPath),
                          MakeStringChecker())
            .AddAttribute("PathLossCache",
                          "Whether to enable the path loss cache",
                          BooleanValue(true),
                          MakeBooleanAccessor(&MlPropagationLossModel::m_pathLossCacheEnabled),
                          MakeBooleanChecker());

    return tid;
}

MlPropagationLossModel::MlPropagationLossModel()
{
    m_mlplNs3Aidl = Create<MlPropagationLossModelNs3AIDL>(NS3_AIDL_ID);

    if (!m_mlplNs3Aidl)
    {
        NS_ABORT_MSG("Error creating MlPropagationLossModelNs3AIDL object");
    }

    m_fastFadingErv =
        CreateObjectWithAttributes<EmpiricalRandomVariable>("Interpolate", BooleanValue(true));

    if (!m_fastFadingErv)
    {
        NS_ABORT_MSG("Error creating EmpiricalRandomVariable object");
    }

    NS_LOG_UNCOND(">> ML Propagation Loss Model initialized.\n"
                  "Please start \"run_ml_propagation_loss_model.py\"");
}

void
MlPropagationLossModel::SetFastFadingCdfPath(const std::string& fastFadingCdfPath)
{
    NS_LOG_FUNCTION(this << fastFadingCdfPath);
    m_fastFadingCdfPath = fastFadingCdfPath;

    if (!m_fastFadingCdfPath.empty())
    {
        LoadFastFadingCdf();
    }
}

std::string
MlPropagationLossModel::GetFastFadingCdfPath() const
{
    return m_fastFadingCdfPath;
}

double
MlPropagationLossModel::DoCalcRxPower(double txPowerDbm,
                                      Ptr<MobilityModel> a,
                                      Ptr<MobilityModel> b) const
{
    // Get the Tx / Rx positions
    Vector txPosition = a->GetPosition();
    Vector rxPosition = b->GetPosition();

    // Get the predicted path loss (> 0) from the ML model (via ns3-ai).
    double pathLossDb = GetPathLossDb(txPosition, rxPosition);

    // Generate a fast-fading value from the CDF
    double fastFadingDb = m_fastFadingErv->GetValue();

    // Final Rx power
    double lossDb = pathLossDb + fastFadingDb;
    double rxPowerDbm = txPowerDbm - lossDb;

    NS_LOG_DEBUG("txPosition=" << txPosition << ", rxPosition=" << rxPosition << ", loss=" << lossDb
                               << " dB");

    return rxPowerDbm;
}

int64_t
MlPropagationLossModel::DoAssignStreams(int64_t stream)
{
    m_fastFadingErv->SetStream(stream);

    return 1;
}

void
MlPropagationLossModel::LoadFastFadingCdf()
{
    NS_LOG_FUNCTION(this);

    NS_LOG_DEBUG("Loading fast-fading CDF from file \"" << m_fastFadingCdfPath << "\"");

    // Read CDF distribution from CSV and configure the corresponding EmpiricalRandomVariable
    CsvReader csvReader(m_fastFadingCdfPath);

    // Skip CSV header
    csvReader.FetchNextRow();

    while (csvReader.FetchNextRow())
    {
        if (csvReader.IsBlankRow())
        {
            continue;
        }

        double x = 0;
        double y = 0;

        if (!csvReader.GetValue(0, x))
        {
            NS_ABORT_MSG("Error reading field \"x\" from the fast-fading CDF CSV file");
        }
        if (!csvReader.GetValue(1, y))
        {
            NS_ABORT_MSG("Error reading field \"y\" from the fast-fading CDF CSV file");
        }

        NS_LOG_DEBUG("Add CDF point: X: " << x << " Y: " << y);

        m_fastFadingErv->CDF(x, y);
    }

    NS_LOG_DEBUG("Finished loading fast-fading CDF");
}

double
MlPropagationLossModel::GetPathLossDb(const Vector& txPosition, const Vector& rxPosition) const
{
    // Check if value is cached and return it
    if (m_pathLossCacheEnabled)
    {
        auto cachedPathLossDb = m_pathLossCache.find({txPosition, rxPosition});

        if (cachedPathLossDb != m_pathLossCache.end())
        {
            return cachedPathLossDb->second;
        }
    }

    // Query ML model
    m_mlplNs3Aidl->SetNodePositions(txPosition, rxPosition);
    double pathLossDb = m_mlplNs3Aidl->GetPredictedPathLossDb();

    // Cache path loss
    if (m_pathLossCacheEnabled)
    {
        m_pathLossCache.insert({{txPosition, rxPosition}, pathLossDb});
    }

    return pathLossDb;
}

} // namespace ns3
