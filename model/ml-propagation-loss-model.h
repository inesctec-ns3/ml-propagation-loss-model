// Copyright (C) 2020-2025, INESC TEC.
// Machine Learning Based Propagation Loss Model for ns-3.
//
// This program is free software: you can redistribute it and/or modify it under the
// terms of the European Union Public Licence v1.2 as published by the European Union.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the European Union Public Licence v1.2 for more details.
//
// You should have received a copy of the European Union Public Licence v1.2 along with this
// program. If not, see <https://www.eupl.eu>.

#pragma once

#include "ml-propagation-loss-model-ns3aidl.h"

#include "ns3/propagation-loss-model.h"
#include "ns3/random-variable-stream.h"
#include "ns3/vector.h"

#include <map>
#include <tuple>

namespace ns3
{

/**
 * ML propagation loss model.
 */
class MlPropagationLossModel : public PropagationLossModel
{
  public:
    /**
     * @brief Get the type ID.
     * @return Object TypeId.
     */
    static TypeId GetTypeId();

    /**
     * Constructor.
     */
    MlPropagationLossModel();

    /**
     * Destructor.
     */
    ~MlPropagationLossModel() override = default;

    // Delete copy constructor and assignment operator to avoid misuse
    MlPropagationLossModel(const MlPropagationLossModel&) = delete;
    MlPropagationLossModel& operator=(const MlPropagationLossModel&) = delete;

    /**
     * Set the path to the CSV file with the Cumulative Distributive Function (CDF)
     * of the fast-fading component of the model.
     *
     * @param fastFadingCdfPath Path to the CSV file with the fast-fading CDF.
     */
    void SetFastFadingCdfPath(const std::string& fastFadingCdfPath);

    /**
     * Get the path to the CSV file with the Cumulative Distributive Function (CDF) of the
     * fast-fading component of the model.
     *
     * @return Path to the CSV file with the fast-fading CDF.
     */
    std::string GetFastFadingCdfPath() const;

  private:
    /**
     * Returns the Rx power using the the ML propagation loss model.
     *
     * @param txPowerDbm Current transmission power (in dBm).
     * @param a Mobility model of the source.
     * @param b Mobility model of the destination.
     * @return Receiving power after adding the path loss
     *         and the fast fading components.
     */
    double DoCalcRxPower(double txPowerDbm,
                         Ptr<MobilityModel> a,
                         Ptr<MobilityModel> b) const override;

    /**
     * Assign a fixed random variable stream number to the random variables used by this model.
     *
     * @param stream First stream index to use.
     * @return Number of stream indices assigned by this model.
     */
    int64_t DoAssignStreams(int64_t stream) override;

    /**
     * Load the fast-fading CDF from a CSV file.
     * The name of the CSV file is stored in m_fastFadingCdfPath.
     */
    void LoadFastFadingCdf();

    /**
     * Get the path loss (> 0) (in dB) for a given set of Tx / Rx positions.
     * If the value is cached, use it. Otherwise, query the ML model (via ns3-ai).
     *
     * @param txPosition Tx position.
     * @param rxPosition Rx position.
     * @return Path loss (> 0) (in dB).
     */
    double GetPathLossDb(const Vector& txPosition, const Vector& rxPosition) const;

    std::string m_fastFadingCdfPath; //!< Path to the CSV file with the fast-fading CDF model
    Ptr<EmpiricalRandomVariable> m_fastFadingErv; //!< Fast-fading empirical random variable
    Ptr<MlPropagationLossModelNs3AIDL> m_mlplNs3Aidl; //!< ML propagation loss model ns3-ai

    bool m_pathLossCacheEnabled; //!< Path loss cache enabled
    mutable std::map<std::tuple<Vector, Vector>, double> m_pathLossCache; //!< Path loss cache
};

} // namespace ns3
