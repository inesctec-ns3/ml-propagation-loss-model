# Machine Learning Based Propagation Loss Model Documentation

This folder contains the documentation of the Machine Learning Based Propagation Loss (MLPL) model.

## Table of Contents

* [Datasets](datasets.md)
* [ML Model Training](ml-model-training.md)
* [MLPL Model Usage in ns-3](mlpl-usage-ns3.md)

## Publications

The list of papers published in international conferences and journals about the MLPL model is available in the following page:

* [List of Publications](publications.md)

More details about the MLPL model are available in the corresponding papers.
