# MLPL Dataset Documentation

The ML models used by the MLPL module are trained with dataset of experimental propagation loss values. This page explains the format and fields of the dataset, along with other useful information.

## Directory Structure

The following diagram shows the directory structure of the MLPL datasets.

```text
datasets/
└─ {DATASET_NAME}/
   ├─ dataset/
   │  ├─ propagation-loss-dataset.csv
   │  └─ wifi-parameters.json (Required by some datasets)
   ├─ dataset-unique/
   │  └─ propagation-loss-unique-dataset.csv
   ├─ ml-model/
   │  └─ {ML_MODEL_INPUT}/
   │     └─ {ML_TRAINING_ALGORITHM}/
   │        ├─ fast-fading-ecdf.csv
   │        ├─ fast-fading-ecdf.pickle
   │        └─ path-loss.pickle
   └─ results
      └─ {ML_MODEL_INPUT}/
         └─ ...
```

Where:

* `{DATASET_NAME}`: The name of the dataset. It is used by MLPL to identify and group all operations related to the dataset.
* `{ML_MODEL_INPUT}`: The ML model input (e.g., "position" for P-MLPL, or "distance" for D-MLPL).
* `{ML_TRAINING_ALGORITHM}`: The ML algorithm used to train the ML model (e.g., "svr", "xbg").

Each dataset is stored in an individual subdirectory of the main `datasets/` directory. Each dataset directory contains the following subdirectories:

| Subdirectory | Description | Generation |
| ------------ | ----------- | ---------- |
| `dataset/` | Original dataset files | Provided by the authors |
| `dataset-unique/` | Unique dataset files | Automatically generated during ML model training |
| `ml-model/` | Generated ML models | Automatically generated during ML model training |
| `results/` | ML model results | Automatically generated during ML model training |

## Dataset Format

An MLPL dataset consists of a CSV file containing samples of the experimental propagation loss and the respective transmitter and receiver node positions, or the distance between them. The dataset CSV file must be named `propagation-loss-dataset.csv`. This file must be saved in the `dataset/` subdirectory.

Each CSV row corresponds to a given sample of related data, which can be a single packet or the average of multiple packets. Using a resolution of one sample per packet is generally recommended, since it typically produces more precise ML models.

Each CSV column (field) represents a specific metric. The following subsections explain the supported fields, including the metric description, the CSV header name and the value unit.

### Node Positions

The positions of the nodes can be provided in one of the following formats.

#### Node Coordinates

The node positions are written in Cartesian coordinates (x, y, z) relative to a given origin point (0, 0, 0). The origin point can be arbitrarily selected by the dataset authors, provided that all coordinates in the dataset are consistent with this reference. Moreover, ns-3 simulations using the MLPL model must use the same origin point in their mobility models, so that the propagation loss estimations are precise.

**NOTE**: This format is only supported in the P-MLPL model.

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| x_tx | Tx node coordinates (X-axis) | Meter (m) |
| y_tx | Tx node coordinates (Y-axis) | Meter (m) |
| z_tx | Tx node coordinates (Z-axis) | Meter (m) |
| x_rx | Rx node coordinates (X-axis) | Meter (m) |
| y_rx | Rx node coordinates (Y-axis) | Meter (m) |
| z_rx | Rx node coordinates (Z-axis) | Meter (m) |

#### Distance between Nodes

In this format, the dataset contains the distance between the Tx node and the Rx node.

**NOTE**: This format is only supported in the D-MLPL model.

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| distance_m | Distance between the Tx and Rx nodes | Meter (m) |

### Propagation Loss

The propagation loss values can be provided in one of the following formats.

#### Direct Propagation Loss

In this format, the dataset contains the propagation loss values directly.

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| loss_db | Total propagation loss | dB |

#### Rx Power

In this format, the dataset contains the power received by the node, assuming a fixed noise.

**NOTE**: This format requires the [Experimental Wireless Parameters File](#experimental-wireless-parameters-file).

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| rx_power_dbm | Rx power | dBm |

#### Signal-to-Noise Ratio (SNR) and Noise

In this format, the dataset contains the Signal-to-Noise Ratio (SNR) and noise received by the node.

**NOTE**: This format requires the [Experimental Wireless Parameters File](#experimental-wireless-parameters-file).

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| snr_db | Signal-to-Noise Ratio (SNR) | dB |
| noise_dbm | Noise | dBm |

### Optional Fields

Apart from the mandatory fields, more information can be optionally added to the samples. Although these fields do not influence the training of the MLPL model, they provide additional information that can be used to calculate other performance metrics related to the model. One example is the precision of the throughput measured in ns-3 simulations.

| CSV Header | CSV Value | CSV Value Unit |
| ---------- | --------- | -------------- |
| throughput_kbps | Throughput of the frame | kbit/s |

## Experimental Wireless Parameters File

In some cases, the dataset requires an additional file describing the experimental wireless parameters. It contains information about the wireless network used in the experimental testbed. This information is used by MLPL to calculate the propagation loss values when the dataset only contains the Rx power, or SNR and noise.

The file must be named `wifi-parameters.json` and is encoded in JSON format. It must be located in the `dataset/` subdirectory alongside the main CSV dataset file.

**NOTE**: This file is only mandatory in some dataset formats. Check each format for more information.

The parameters required by MLPL are listed in the following table.

| Parameter | Value | Unit | JSON Value Type |
| --------- | ----- | ---- | --------------- |
| tx_power_dbm | Tx power | dBm | float |
| tx_gain_dbi | Tx antenna gain | dBi | float |
| rx_gain_dbi | Rx antenna gain | dBi | float |
| frequency_mhz | Channel center frequency | MHz | float |
| bandwidth_mhz | Channel bandwidth | MHz | float |
| wifi_standard | Wi-Fi Standard (e.g., "802.11a") | - | str |

## Example Dataset

The MLPL module has two example datasets:

* [Distance Dataset Example](../datasets/distance-dataset-example/)
* [Position Dataset Example](../datasets/position-dataset-example/)

They are used to test the MLPL module. They can also be used as references to build custom datasets.
