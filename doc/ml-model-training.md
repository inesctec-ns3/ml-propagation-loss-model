# Machine Learning Model Training

This page explains how to train and run the Machine Learning (ML) models of the path loss and fast-fading.

## ML Model Training

The path loss and fast-fading ML models supporting the MLPL model are trained using the `ml_model/train_ml_propagation_loss_model.py` script.

They are trained with a given dataset, which must be stored in the [datasets/models](datasets/models) directory. Refer to the [dataset documentation](../datasets/README.md) for details about the datasets.

The ML models should be trained before running ns-3 simulations. To train the ML models for a given dataset, run the following command:

```shell
python ml_model/train_ml_propagation_loss_model.py --dataset=DATASET --mlpl-model=MLPL_MODEL --ml-algorithm=ML_ALGORITHM
```

The arguments of the Python script correspond to the ML model that shall be trained and allow the selection of the dataset, the MLPL model and the ML algorithm.

**NOTE**: The ML model should be trained in the same PC where the ns-3 simulations will run. This avoids possible compatibility problems due to different PC architectures and Python versions.

## ML Model Files

After the ML model training is finished, the ML model is saved in the directory `DATASET/ml_model/`, within the corresponding the dataset's main directory. The ML model is composed of multiple files that define the path loss model and the fast-fading distribution.
