# Publications about MLPL

The publications about the MLPL model are listed in this page. Please cite them as follows.

* Eduardo Nuno Almeida, Helder Fontes, Rui Campos, and Manuel Ricardo. 2023. Position-Based Machine Learning Propagation Loss Model Enabling Fast Digital Twins of Wireless Networks in ns-3. In Proceedings of the 2023 Workshop on ns-3 (WNS3 '23). ACM, 69–77. <https://doi.org/10.1145/3592149.3592150>

  📊 [Presentation Slides](https://drive.inesctec.pt/s/YQ5x8enaWNJT8rj)
  🎞️ [Presentation Video](https://drive.inesctec.pt/s/FrBmzPWf7wKWE7Q)

  🏆 Best Paper Award
  🏆 ACM Artifacts Available Badge

* Eduardo Nuno Almeida, Mohammed Rushad, Sumanth Reddy Kota, Akshat Nambiar, Hardik L. Harti, Chinmay Gupta, Danish Waseem, Gonçalo Santos, Helder Fontes, Rui Campos, and Mohit P. Tahiliani. 2022. Machine Learning Based Propagation Loss Module for Enabling Digital Twins of Wireless Networks in ns-3. In Proceedings of the Workshop on ns-3 (WNS3 2022). ACM, 17–24. <https://doi.org/10.1145/3532577.3532607>

  📊 [Presentation Slides](https://drive.inesctec.pt/s/AWSYkkjMPcQwcJi)
  🎞️ [Presentation Video](https://drive.inesctec.pt/s/JD2DzerZ7MYgkMA)
