# Machine Learning Based Propagation Loss Model Usage in ns-3

This page explains how to use the Machine Learning based Propagation Loss (MLPL) model in an ns-3 simulation script.

## Configuration of the MLPL Model

The MLPL model is implemented similarly to other propagation loss models in ns-3, by inheriting from the generic propagation loss model class. Therefore, the configuration and installation of the MLPL model is similar to existing propagation loss models in ns-3.

The following code snippet shows how to configure and install the MLPL model.

```cpp
// MLPL model
YansWifiChannelHelper wifiChannel;
wifiChannel.AddPropagationLoss("ns3::MlPropagationLossModel",
                               "FastFadingCdfPath",
                               StringValue(fastFadingCdfPath),
                               // Optional attributes (if needed)
                               "PathLossCache",
                               BooleanValue(true));

// Wi-Fi PHY and MAC
YansWifiPhyHelper wifiPhy;
wifiPhy.SetChannel(wifiChannel.Create());

WifiMacHelper wifiMac;
NetDeviceContainer netDevices = wifi.Install(wifiPhy, wifiMac, nodes);
```

## MLPL Model's Attributes

The MLPL model defines attributes to configure the model. The attributes and their properties are listed in the following table:

| Attribute | Help | Value Type | Default Value |
| --------- | ---- | ---------- | ------------- |
| `FastFadingCdfPath` | Absolute path to the CSV file with the fast-fading CDF model | String | N/A |
| `PathLossCache` | Whether to enable the path loss cache | Boolean | True |

The `FastFadingCdfPath` string attribute is mandatory and should be set to the file generated during the training of the ML models. The remaining attributes are optional. Refer to the MLPL's source code for more information.

## Activate the ML Models

The propagation loss for a given packet sent by a transmitter to a receiver is calculated by the ML models trained for a given dataset. The inputs and outputs of the ML models are exchanged between the ns-3 process running the simulation and a background process running the ML models. The exchange of data is controlled by the [ns3-ai](https://apps.nsnam.org/app/ns3-ai/) module.

To activate the ML models, open a new terminal and run the following command:

```shell
python ml_model/run_ml_propagation_loss_model.py --dataset=DATASET --ml-input=ML_INPUT --ml-algorithm=ML_ALGORITHM
```

The arguments of the Python script correspond to the ML model that shall be used in the simulation and allow the selection of the dataset, the MLPL model and the ML algorithm.

**NOTE**: This terminal must be running in the background while the ns-3 simulation is running, including the test suite and the examples.
